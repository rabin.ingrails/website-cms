import { MongoClient } from 'mongodb';

async function convertSectionField() {
    const client = new MongoClient(process.env.DATABASE_URL!);

    try {
        await client.connect();
        const db = client.db(); // Automatically uses the database from the connection string
        const collection = db.collection('Content'); // Replace 'Content' with your actual collection name


        // Use the pipeline to update the documents
        const result = await collection.updateMany(
            { section: { $type: "string" } }, // Match documents where `section` is a string
            [
                {
                    $set: {
                        section: { $toInt: "$section" } // Convert `section` to integer
                    }
                }
            ]
        );

        console.log(`Matched ${result.matchedCount} documents and modified ${result.modifiedCount} documents`);

    } finally {
        await client.close();
    }
}

 convertSectionField();