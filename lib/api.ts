import {
    CardContent,
    Content,
    Gallery,
    Page,
    RegularContent,
    TabContent,
    User,
} from '@prisma/client';
import prisma from '../lib/prisma';
import * as argon2 from 'argon2';
import * as jose from 'jose';
import { cookies } from 'next/headers';
import { unstable_cache } from 'next/cache';
import { revalidateFrontEndApis } from '@/actions/content.actions';

export type Response = {
    success: boolean;
    message: any;
    res?: any;
};
type UpdateContent = Omit<Content, 'createdAt' | 'updatedAt'> & {
    tab?: Omit<TabContent, 'createdAt' | 'updatedAt'>[];
    card?: Omit<CardContent, 'createdAt' | 'updatedAt'>[];
    regular?: Omit<CardContent, 'createdAt' | 'updatedAt'>[];
};

// type AddContent = Omit<Content, 'createdAt' | 'updatedAt'> & {
//     tab?: Omit<TabContent, 'id'|'contentId'| 'createdAt' | 'updatedAt'>[];
//     card?: Omit<CardContent,'id' | 'contentId'|'createdAt' | 'updatedAt'>[];
//     regular?: Omit<CardContent, 'id'|'contentId'|'createdAt' | 'updatedAt'>[];
// };

const JWT_SECRET = new TextEncoder().encode(process.env.JWT_SECRET);

const getErrorMessage = (error: unknown): string => {
    let message: string;

    if (error instanceof Error) {
        message = error.message;
    } else if (error && typeof error === 'object' && 'message' in error) {
        message = String(error.message);
    } else if (typeof error === 'string') {
        message = error;
    } else {
        message = 'Something went wrong';
    }

    return message;
};

export const createUserService = async (
    user: Omit<User, 'id' | 'createdAt' | 'updatedAt'>
): Promise<Response> => {
    try {
        const hash = await argon2.hash(user.password);

        const newUser = await prisma.user.create({
            data: {
                email: user.email,
                password: hash,
                projectKey: user.projectKey,
            },
        });
        return {
            success: true,
            message: 'User created successfully',
            res: newUser,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const signInUserService = async (
    user: Omit<User, 'id' | 'projectKey' | 'createdAt' | 'updatedAt'>
): Promise<Response> => {
    try {
        const findUser = await prisma.user.findFirst({
            where: {
                email: user.email,
            },
        });

        if (!findUser) {
            return {
                success: false,
                message: 'User not found',
            };
        }
        //*check password
        const verify = await argon2.verify(findUser.password, user.password);
        if (!verify) {
            return {
                success: false,
                message: 'Incorrect password',
            };
        }

        //*create token
        const token = await new jose.SignJWT({ userId: findUser.id })
            .setProtectedHeader({ alg: 'HS256' })
            .setExpirationTime('7d')
            .sign(JWT_SECRET);

        cookies().set('access_token', token);

        return {
            success: true,
            message: 'Sign in successful',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

const verifyToken = async (accessToken: any) => {
    if (!accessToken || !accessToken?.value) {
        return;
    }
    const { payload }: any = await jose.jwtVerify(
        accessToken?.value,
        JWT_SECRET
    );
    return payload?.userId;
};

const getUserId = async () => {
    const access_token = cookies().get('access_token');
    const userId = await verifyToken(access_token);
    return userId;
};

export const getPageService = async (includeContent = true) => {
    const userId = await getUserId();

    return await prisma.page.findMany({
        where: {
            userId,
        },
        include: {
            content: includeContent,
            subPages: true,
        },
    });
};
export const getSinglePageService = async (pageId: string) => {
    return await prisma.page.findFirst({
        where: {
            id: pageId,
        },
    });
};

export const getPageContentService = async (pageId: string) => {
    try {
        const page = await getSinglePageService(pageId);
        const content = await prisma.content.findMany({
            where: {
                pageId,
            },
            include: {
                tab: true,
                card: true,
                regular: true,
            },
            orderBy: {
                section: 'asc',
            },
        });

        return {
            success: true,
            message: 'Content fetched successfully',
            res: { page, content },
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const getPageContentByIdService = async (contentId: string) => {
    try {
        const content = await prisma.content.findUnique({
            where: {
                id: contentId,
            },
            include: {
                tab: {
                    orderBy: {
                        order: 'asc',
                    },
                },
                card: {
                    orderBy: {
                        order: 'asc',
                    },
                },
                regular: {
                    orderBy: {
                        order: 'asc',
                    },
                },
            },
        });

        return {
            success: true,
            message: 'Content fetched successfully',
            res: content,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

// export const addContentService = async (content: Omit<Content, 'id' | 'createdAt' | 'updatedAt'>) => {
//     try {
//         const { pageId, ...rest } = content
//         const added = await prisma.content.create({
//             data: {
//                 pageId,
//                 title: rest.title,
//                 section: rest.section,
//             },
//         });

//         return {
//             success: true,
//             message: 'Content added successfully',
//         }
//     } catch (err: unknown) {
//         return {
//             success: false,
//             message: getErrorMessage(err),
//         }
//     }
// };

export const addContentService = async (
    content: Omit<Content, 'id' | 'section' | 'createdAt' | 'updatedAt'>
) => {
    try {
        //*auto calculate page section from pageid
        const numberOfContentInPage = await prisma.content.count({
            where: {
                pageId: content.pageId,
            },
        });

        const createdContent = await prisma.content.create({
            data: {
                ...content,
                section: numberOfContentInPage + 1,
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Content added successfully',
            data: createdContent,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const deleteContentSectionService = async (contentId: string) => {
    try {
        const deleted = await prisma.content.delete({
            where: {
                id: contentId,
            },
        });
        //*update order
        await prisma.content.updateMany({
            where: {
                pageId: deleted.pageId,
                section: {
                    gt: deleted.section, // Update items that come after the deleted section
                },
            },
            data: {
                section: {
                    decrement: 1, // Decrease the section number by 1
                },
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Content deleted successfully',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

const createManyTabs = (tabs: TabContent[], contentId: string) => {
    return prisma.tabContent.createMany({
        data: tabs.map((tab) => ({ ...tab, contentId })),
    });
};

export const updateContentService = async (content: UpdateContent) => {
    try {
        const { id, tab, card, regular, ...rest } = content;
        console.log('🚀 ~ updateContentService ~ content:', content);

        //*perform upsert for tab only
        if (!tab?.length)
            return {
                success: true,
                message: 'ds',
            };
        const upsertTab = tab[0]?.id
            ? await prisma.tabContent.upsert({
                  where: { id: tab[0].id },
                  update: { ...tab[0] },
                  create: { ...tab[0], contentId: id },
              })
            : await prisma.tabContent.create({
                  data: { ...tab[0], contentId: id },
              });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Content updated successfully',
        };
    } catch (err: unknown) {
        console.log('🚀 ~ updateContentService ~ err:', err);
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const addPageService = async (
    page: Omit<Page, 'userId' | 'id' | 'createdAt' | 'updatedAt'>
) => {
    const userId = await getUserId();
    // console.log(page)
    // return {
    //     success:true,
    //     message:'df'
    // }
    try {
        const newPage = await prisma.page.create({
            data: {
                title: page.title,
                slug: page.slug,
                parentId: page.parentId,
                metaTags: page.metaTags,
                userId,
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Content updated successfully',
            res: newPage,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const updatePageService = async (
    page: Omit<Page, 'userId' | 'createdAt' | 'updatedAt'>
) => {
    try {
        const updatedPage = await prisma.page.update({
            where: {
                id: page.id,
            },
            data: {
                title: page.title,
                slug: page.slug,
                metaTags: page.metaTags,
                parentId: page.parentId,
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Page updated successfully',
            res: updatedPage,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const createTabService = async (
    tab: Omit<TabContent, 'id' | 'order' | 'createdAt' | 'updatedAt'>[]
) => {
    try {
        //*auto calculate page section from pageid
        const numberOfTabs = await prisma.tabContent.count({
            where: {
                contentId: tab[0].contentId,
            },
        });
        const newTab = await prisma.tabContent.createMany({
            data: tab.map((item, index) => ({
                ...item,
                order: numberOfTabs + 1 + index,
            })),
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Tab created successfully',
            res: newTab,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const updateBulkTabService = async (
    tab: Omit<TabContent, 'createdAt' | 'updatedAt'>[]
) => {
    try {
        const updates = tab?.map((item) =>
            prisma.tabContent.update({
                where: {
                    id: item.id,
                },
                data: {
                    label: item.label,
                    link: item.link,
                    desc: item.desc,
                    img: item.img,
                },
            })
        );

        const results = await prisma.$transaction(updates);
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Tab updated successfully',
            res: results,
        };
    } catch (err: unknown) {
        console.log('🚀 ~ updateTabService ~ err:', err);
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const updateTabService = async (
    tab: Omit<TabContent, 'order' | 'createdAt' | 'updatedAt'>
) => {
    try {
        const updatedTab = await prisma.tabContent.update({
            where: {
                id: tab.id,
            },
            data: {
                label: tab.label,
                link: tab.link,
                desc: tab.desc,
                img: tab.img,
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Tab updated successfully',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const removeTabService = async (tabId: string) => {
    try {
        const results = await prisma.tabContent.delete({
            where: {
                id: tabId,
            },
        });
        //*update order
        await prisma.tabContent.updateMany({
            where: {
                contentId: results.contentId,
                order: {
                    gt: results.order, // Update items that come after the deleted section
                },
            },
            data: {
                order: {
                    decrement: 1, // Decrement the order by 1
                },
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Tab deleted successfully',
        };
    } catch (err: unknown) {
        console.log('🚀 ~ removeTabService ~ err:', err);
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const createCardService = async (
    card: Omit<CardContent, 'id' | 'createdAt' | 'updatedAt'>[]
) => {
    try {
        //*calculate card count
        const numberOfCards = await prisma.cardContent.count({
            where: {
                contentId: card[0].contentId,
            },
        });
        const newCard = await prisma.cardContent.createMany({
            data: card.map((item, index) => ({
                ...item,
                order: numberOfCards + 1 + index,
            })),
        });

        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Card created successfully',
            res: newCard,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const removeCardService = async (cardId: string) => {
    console.log('🚀 ~ removeCardService ~ cardId:', cardId);
    try {
        const results = await prisma.cardContent.delete({
            where: {
                id: cardId,
            },
        });
        //*update order
        await prisma.cardContent.updateMany({
            where: {
                contentId: results.contentId,
                order: {
                    gt: results.order, // Update items that come after the deleted section
                },
            },
            data: {
                order: {
                    decrement: 1, // Decrement the order by 1
                },
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Card deleted successfully',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const updateCardService = async (
    card: Omit<CardContent, 'order' | 'createdAt' | 'updatedAt'>
) => {
    try {
        const updatedCard = await prisma.cardContent.update({
            where: {
                id: card.id,
            },
            data: {
                title: card.title,
                subTitle: card.subTitle,
                link: card.link,
                desc: card.desc,
                img: card.img,
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Card updated successfully',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const createRegularService = async (
    regular: Omit<RegularContent, 'id' | 'createdAt' | 'updatedAt'>[]
) => {
    try {
        //*calculate card count
        const numberOfRegular = await prisma.regularContent.count({
            where: {
                contentId: regular[0].contentId,
            },
        });
        const newRegular = await prisma.regularContent.createMany({
            data: regular.map((item, index) => ({
                ...item,
                order: numberOfRegular + 1 + index,
            })),
        });

        revalidateFrontEndApis();
        return {
            success: true,
            message: 'Regular created successfully',
            res: newRegular,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const updateRegularService = async (
    regular: Omit<RegularContent, 'order' | 'createdAt' | 'updatedAt'>
) => {
    try {
        const updatedRegular = await prisma.regularContent.update({
            where: {
                id: regular.id,
            },
            data: {
                desc: regular.desc,
                img: regular.img,
            },
        });
        revalidateFrontEndApis();
        return {
            success: true,
            message: 'Regular updated successfully',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const removeRegularService = async (regularId: string) => {
    try {
        const results = await prisma.regularContent.delete({
            where: {
                id: regularId,
            },
        });
        //*update order
        await prisma.regularContent.updateMany({
            where: {
                contentId: results.contentId,
                order: {
                    gt: results.order, // Update items that come after the deleted section
                },
            },
            data: {
                order: {
                    decrement: 1, // Decrement the order by 1
                },
            },
        });
        revalidateFrontEndApis();
        return {
            success: true,
            message: 'Regular deleted successfully',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const updateTitleService = async ({
    title,
    subTitle,
    contentId,
}: {
    title: string;
    subTitle: string;
    contentId: string;
}) => {
    try {
        const updated = await prisma.content.update({
            where: {
                id: contentId,
            },
            data: {
                title: title,
                subTitle: subTitle,
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Title updated successfully',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const removePageService = async (pageId: string) => {
    try {
        const results = await prisma.page.delete({
            where: {
                id: pageId,
            },
        });
        revalidateFrontEndApis();

        return {
            success: true,
            message: 'Page deleted successfully',
        };
    } catch (err: unknown) {
        console.log('🚀 ~ removePageService ~ err:', err);
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const listMetaDataByProjectKeyService = unstable_cache(
    async (projectKey: string) => {
        try {
            const results = await prisma.user.findFirst({
                where: {
                    projectKey: projectKey,
                },
                select: {
                    page: true,
                },
            });
            return {
                success: true,
                message: 'Pages fetched successfully',
                res: results?.page,
            };
        } catch (err: unknown) {
            return {
                success: false,
                message: getErrorMessage(err),
            };
        }
    },
    ['meta'],
    {
        revalidate: 10,
    }
);

export const listContentPerPageService = unstable_cache(
    async (projectKey: string, pageSlug: string) => {
        try {
            const results = await prisma.content.findMany({
                where: {
                    page: {
                        slug: pageSlug,
                        user: {
                            projectKey: projectKey,
                        },
                    },
                },
                include: {
                    card: {
                        orderBy: {
                            order: 'asc',
                        },
                    },
                    regular: {
                        orderBy: {
                            order: 'asc',
                        },
                    },
                    tab:  {
                        orderBy: {
                            order: 'asc',
                        },
                    },
                },
                orderBy: {
                    section: 'asc',
                },
            });
            return {
                success: true,
                message: 'Content fetched successfully',
                res: results,
            };
        } catch (err: unknown) {
            return {
                success: false,
                message: getErrorMessage(err),
            };
        }
    },
    ['page-content'],
    {
        revalidate: 10,
    }
);

export const updateGalleryService = async (imgs: string[]) => {
    const userId = await getUserId();

    try {
        const updatedGallery = await prisma.gallery.createMany({
            data: imgs.map((img) => {
                return {
                    img,
                    userId,
                };
            }),
        });
        return {
            success: true,
            message: 'Gallery updated successfully',
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const getUserGalleryService = async () => {
    const userId = await getUserId();
    try {
        const results = await prisma.gallery.findMany({
            where: {
                userId,
            },
            orderBy: {
                createdAt: 'desc',
            }
        });
        return {
            success: true,
            message: 'Gallery fetched successfully',
            res: results,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const getUserInfoService = async (): Promise<{
    success: boolean;
    message: string;
    res?: Pick<User, 'id' | 'email' | 'projectKey'> | null;
}> => {
    const userId = await getUserId();
    try {
        const results = await prisma.user.findUnique({
            where: {
                id: userId,
            },
            select: {
                email: true,
                id: true,
                projectKey: true,
            },
        });
        return {
            success: true,
            message: 'User fetched successfully',
            res: results,
        };
    } catch (err: unknown) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};
export const updateContentOrderService = async (
    contentOrders: { id: string; order: number }[] | undefined
): Promise<{ success: boolean; message: string }> => {
    try {
        if (!contentOrders) throw new Error('No content orders provided');
        const updatePromises = contentOrders?.map(({ id, order }) =>
            prisma.content.update({
                where: { id },
                data: { section: Number(order) },
            })
        );

        // Await all updates to finish
        await Promise.all(updatePromises);

        return {
            success: true,
            message: 'Contents order updated successfully',
        };
    } catch (err) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const updateTabOrderService = async (
    tabOrders:
        | {
              id: string;
              order: number;
          }[]
        | undefined
): Promise<{ success: boolean; message: string }> => {
    try {
        if (!tabOrders) throw new Error('No content orders provided');
        const updatePromises = tabOrders?.map(({ id, order }) =>
            prisma.tabContent.update({
                where: { id },
                data: { order: Number(order) },
            })
        );

        // Await all updates to finish
        await Promise.all(updatePromises);

        return {
            success: true,
            message: 'Tab order updated successfully',
        };
    } catch (err) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const updateCardOrderService = async (
    cardOrders:
        | {
              id: string;
              order: number;
          }[]
        | undefined
): Promise<{ success: boolean; message: string }> => {
    try {
        if (!cardOrders) throw new Error('No content orders provided');
        const updatePromises = cardOrders?.map(({ id, order }) =>
            prisma.cardContent.update({
                where: { id },
                data: { order: Number(order) },
            })
        );        // Await all updates to finish
        await Promise.all(updatePromises);

        return {
            success: true,
            message: 'Card order updated successfully',
        };
    } catch (err) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};

export const updateRegularOrderService = async (
    regularOrders:
        | {
              id: string;
              order: number;
          }[]
        | undefined
): Promise<{ success: boolean; message: string }> => {
    try {
        if (!regularOrders) throw new Error('No content orders provided');
        const updatePromises = regularOrders?.map(({ id, order }) =>
            prisma.regularContent.update({
                where: { id },
                data: { order: Number(order) },
            })
        );        // Await all updates to finish
        await Promise.all(updatePromises);    
        return {
            success: true,
            message: 'Regular order updated successfully',
        };
    } catch (err) {
        return {
            success: false,
            message: getErrorMessage(err),
        };
    }
};