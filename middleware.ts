import { NextResponse } from 'next/server';
import type { NextRequest } from 'next/server';
import * as jose from 'jose';

const JWT_SECRET = new TextEncoder().encode(process.env.JWT_SECRET);

const verifyToken = async (accessToken: any) => {
    if (!accessToken || !accessToken?.value) {
        return;
    }
    const { payload }: any = await jose.jwtVerify(
        accessToken?.value,
        JWT_SECRET
    );
    return payload?.userId;
};

export async function middleware(request: NextRequest) {
    const routePrefix = process.env.NEXT_PUBLIC_PREFIX;
    const baseURL = process.env.NEXT_PUBLIC_BASE_URL
    console.log("🚀 ~ middleware ~ baseURL:", baseURL)
    const signInPage = routePrefix ? '/' + routePrefix + '/signin' : '/signin';
    const dashboardPage = routePrefix
        ? '/' + routePrefix + '/dashboard'
        : '/dashboard';
    let accessToken = request.cookies.get('access_token');

    if (!accessToken || !accessToken?.value) {
      
        if (request.nextUrl.pathname === signInPage) {
            return NextResponse.next();
        }

        return NextResponse.redirect(baseURL+signInPage);
    }
    let userId = null;
    try {
        userId = await verifyToken(accessToken);
    } catch (error) {
        console.log('🚀 ~ middleware ~ error:', error);
        return NextResponse.redirect(baseURL+signInPage);
    }

    if (!userId) {
        return NextResponse.redirect(baseURL+signInPage);
    }
    console.log('🚀 found:', accessToken);

    if (request.nextUrl.pathname === signInPage) {
        return NextResponse.redirect(baseURL+dashboardPage);
    }
    return NextResponse.next();
}

// See "Matching Paths" below to learn more
export const config = {
    // matcher: ['/dashboard/:path*', '/api/:path*'],
    matcher: [
        // '/signin',
        '/dashboard/:path*',
        // '/api/:path*',
        // '/((?!api|_next/static|_next/image|favicon.ico).*)',
    ],
};
