import { User } from '@prisma/client';
import { create } from 'zustand';

type UserState = {
    user: Pick<User, 'email' | 'projectKey' | 'id'>;
    setUser: (user: Pick<User, 'email' | 'projectKey' | 'id'>) => void;
};
export const useUserInfoStore = create<UserState>((set) => ({
    user: {
        email: '',
        projectKey: '',
        id: '',
    },
    setUser: (user) => set({ user }),
}));
