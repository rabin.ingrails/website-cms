import type { Page, Content as PrismaContent } from '@prisma/client';

export type Content = Omit<PrismaContent, 'id' | 'userId'>;
export type PageWithSubPages = Page & { subPages: Page[] };
export  type FileWithPreview = File & {
    preview: string;
};