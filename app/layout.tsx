import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import NextTopLoader from 'nextjs-toploader';
import { Toaster } from '@/components/ui/sonner';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
    title: 'Veda CMS',
    description: '',
};

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
            <body className={inter.className}>
                <NextTopLoader
                    height={4}
                    color="#FF0000"
                    showSpinner={false}
                    zIndex={9999999}
                />

                {children}
                <Toaster richColors 
                position='top-center'
                />
            </body>
        </html>
    );
}
