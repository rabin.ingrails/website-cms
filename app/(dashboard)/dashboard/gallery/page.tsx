import React from 'react';
import GalleryComponent from '@/components/gallery/gallery-component';
import { getUserGalleryService } from '@/lib/api';
import {GalleryViewerLarge} from '@/components/gallery/gallery-viewer';


async function Page() {
    const res = await getUserGalleryService();
    return (
        <div>
            <h2 className="scroll-m-20  text-3xl font-semibold tracking-tight ">
                Gallery
            </h2>
            <GalleryComponent />

            <GalleryViewerLarge gallery={res.res} />
        </div>
    );
}

export default Page;
