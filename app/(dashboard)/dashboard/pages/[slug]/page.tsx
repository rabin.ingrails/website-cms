
import { getPageContentService, getUserGalleryService } from '@/lib/api';

import PageTab from '@/components/tabs/page-tab';

async function Page({
    params,
    searchParams,
}: {
    params: { slug: string };
    searchParams: { page: string };
}) {
    const res = await getPageContentService(params.slug);
    const gallery = await getUserGalleryService()
    
    return <PageTab key={res.res?.content?.length} contents={res.res?.content }  page={res.res?.page}  gallery={gallery.res}/>;
}

export default Page;
