import React, { useCallback, useEffect, useRef } from 'react';
import {
    Card,
    CardContent,
    CardHeader,
    CardTitle,
    CardDescription,
} from '@/components/ui/card';
import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselPrevious,
    CarouselNext,
} from '@/components/ui/carousel';
import Image from 'next/image';
import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { GripVertical } from 'lucide-react';
import EditCardForm from '@/components/forms/edit-card-form';
import { CardContent as CardContentType } from '@prisma/client';
import { updateCardOrderAction } from '@/actions/content.actions';
import { toast } from 'sonner';

// Define the drag item type
const ItemTypes = {
    CARD: 'card',
};

interface DraggableCardProps {
    card: CardContentType;
    index: number;
    moveCard: (dragIndex: number, hoverIndex: number) => void;
}

const DraggableCard: React.FC<DraggableCardProps> = ({
    card,
    index,
    moveCard,
}) => {
    const cardRef = useRef(null);
    const dragHandleRef = useRef(null);

    const [{ isDragging }, drag, preview] = useDrag({
        type: ItemTypes.CARD,
        item: { index },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
        canDrag: (monitor) => {
            return monitor.getInitialClientOffset() !== null;
        },
    });

    const [{ isOver }, drop] = useDrop({
        accept: ItemTypes.CARD,
        hover: (item: { index: number }) => {
            if (item.index === index) {
                return;
            }
            moveCard(item.index, index);
            item.index = index;
        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
        }),
    });
    drop(preview(cardRef));

    return (
        <div
            ref={cardRef}
            className={`border rounded-lg p-4 h-fit relative transition-all ${
                isDragging ? 'opacity-50 scale-105' : ''
            } ${isOver ? 'border-blue-500' : ''}`}
        >
            <div className="flex items-center gap-2 justify-between mb-2">
                <div
                    className="cursor-grab active:cursor-grabbing"
                    ref={drag(dragHandleRef) as any}
                >
                    <GripVertical className="h-4 w-4 text-gray-400" />
                </div>
                <div className="">
                    <EditCardForm card={card} />
                </div>
            </div>
            {!!card.img.length && (
                <Carousel className="border rounded-lg">
                    <CarouselContent>
                        {card.img?.map((img) => (
                            <CarouselItem key={img} className="h-60 w-full">
                                <Image
                                    src={img}
                                    alt={'card-img'}
                                    height={400}
                                    width={400}
                                    className="object-contain rounded-lg h-full"
                                />
                            </CarouselItem>
                        ))}
                    </CarouselContent>
                    {card.img.length > 1 && (
                        <>
                            <CarouselPrevious />
                            <CarouselNext />
                        </>
                    )}
                </Carousel>
            )}
            <CardTitle className="text-xl mt-4">{card.title}</CardTitle>
            <CardDescription>{card.subTitle}</CardDescription>
            <div
                className="mt-2 max-h-40 overflow-y-auto overflow-x-hidden"
                dangerouslySetInnerHTML={{ __html: card.desc }}
            ></div>
        </div>
    );
};

function CardListComponent({
    cards: initialCards,
}: {
    cards: CardContentType[];
}) {
    const [cards, setCards] = React.useState(initialCards);
    useEffect(() => {
        setCards(initialCards);
    }, [initialCards]);

    const moveCard = useCallback((dragIndex: number, hoverIndex: number) => {
        setCards((prevCards) => {
            const newCards = [...prevCards];
            const draggedCard = newCards[dragIndex];
            newCards.splice(dragIndex, 1);
            newCards.splice(hoverIndex, 0, draggedCard);
            return newCards?.map((card, index) => ({
                ...card,
                order: index + 1,
            }));
        });
    }, []);

    const handleUpdateOrder = async () => {
        //*check if previous order and current order are same, if not call api
        const currentOrder = cards.map((card) => ({
            id: card.id,
            order: card.order,
        }));
        const previousOrder = initialCards.map((card) => ({
            id: card.id,
            order: card.order,
        }));
        // Check if order changed
        const hasOrderChanged = currentOrder.some((curr, index) => {
            const prev = previousOrder[index];
            return prev.id !== curr.id || prev.order !== curr.order;
        });

        if (!hasOrderChanged) {
            return;
        }

        //*call POST API
        try {
            const res = await updateCardOrderAction(
                cards?.map((i, index) => ({ id: i.id, order: i.order }))
            );
            if (res.success) {
                toast.success(res.message);
            } else {
                toast.error(res.message);
            }
        } catch (err: any) {
            console.log('🚀 ~ err ~ err:', err);
            toast.error(err.message);
        }
    };
    return (
        <Card>
            <CardHeader>
                <CardTitle className="flex justify-between items-center">
                    Card
                </CardTitle>
            </CardHeader>
            <CardContent
                className="grid grid-cols-3 gap-4"
                onDragEnd={handleUpdateOrder}
            >
                {cards?.map((card, index) => (
                    <DraggableCard
                        key={card.id}
                        card={card}
                        index={index}
                        moveCard={moveCard}
                    />
                ))}
            </CardContent>
        </Card>
    );
}

export default CardListComponent;
