import {
    getPageContentByIdService,
    getPageContentService,
    getUserGalleryService,
} from '@/lib/api';

import PageContentComponent from './page-content-component';

async function Page({
    params,
    searchParams,
}: {
    params: { content: string };
    searchParams: { page: string };
}) {
    const res = await getPageContentByIdService(params.content);
    const gallery = await getUserGalleryService();

    return <PageContentComponent content={res?.res} gallery={gallery.res} />;
}

export default Page;
