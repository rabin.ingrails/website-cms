import React, { useCallback, useEffect } from 'react';
import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card';
import { TabContent } from '@prisma/client';
import { Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs';
import NoContentMessage from './no-content-message';
import EditTabForm from '@/components/forms/edit-tab-form';
import { GripVertical } from 'lucide-react';
import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { updateTabOrderAction } from '@/actions/content.actions';
import { toast } from 'sonner';

const ItemTypes = {
    TAB: 'tab',
};

interface DraggableTabProps {
    tab: TabContent;
    index: number;
    moveTab: (dragIndex: number, hoverIndex: number) => void;
}

const DraggableTab: React.FC<DraggableTabProps> = ({ tab, index, moveTab }) => {
    const [{ isDragging }, drag] = useDrag({
        type: ItemTypes.TAB,
        item: { index },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    });

    const [, drop] = useDrop({
        accept: ItemTypes.TAB,
        hover: (item: { index: number }) => {
            if (item.index === index) {
                return;
            }
            moveTab(item.index, index);
            item.index = index;
        },
    });

    return (
        <div
            ref={(node) => drag(drop(node)) as any}
            className={`flex items-center gap-2 w-full ${
                isDragging ? 'opacity-50' : ''
            }`}
        >
            <GripVertical className="h-4 w-4 cursor-grab active:cursor-grabbing" />
            <TabsTrigger value={tab.id} className="flex-1 text-left">
                {tab.label}
            </TabsTrigger>
        </div>
    );
};

function TabListComponent({ tabs: initialTabs }: { tabs: TabContent[] }) {
    const [tabs, setTabs] = React.useState(initialTabs);
    useEffect(() => {
        setTabs(initialTabs);
    }, [initialTabs]);

    const handleUpdateOrder = async () => {
        //*check if previous order and current order are same, if not call api
        const currentOrder = tabs.map((tab) => ({
            id: tab.id,
            order: tab.order,
        }));
        const previousOrder = initialTabs.map((tab) => ({
            id: tab.id,
            order: tab.order,
        }));
        // Check if order changed
        const hasOrderChanged = currentOrder.some((curr, index) => {
            const prev = previousOrder[index];
            return prev.id !== curr.id || prev.order !== curr.order;
        });

        if (!hasOrderChanged) {
            return;
        }
        //*call POST API
        try {
            const res = await updateTabOrderAction(
                tabs?.map((i, index) => ({ id: i.id, order: i.order }))
            );
            console.log('called ', res);
            if (res.success) {
                toast.success(res.message);
            } else {
                toast.error(res.message);
            }
        } catch (err: any) {
            console.log('🚀 ~ err ~ err:', err);
            toast.error(err.message);
        }
    };

    const moveTab = useCallback((dragIndex: number, hoverIndex: number) => {
        setTabs((prevTabs) => {
            const newTabs = [...prevTabs];
            const draggedTab = newTabs[dragIndex];
            newTabs.splice(dragIndex, 1);
            newTabs.splice(hoverIndex, 0, draggedTab);
            return newTabs.map((tab, index) => ({ ...tab, order: index + 1 }));
        });
    }, []);

    return (
        <Card>
            <CardHeader>
                <CardTitle className="flex justify-between items-center">
                    Tab
                </CardTitle>
            </CardHeader>
            <CardContent>
                <div onDragEnd={handleUpdateOrder}>
                    {tabs.length ? (
                        <Tabs className="flex" defaultValue={tabs[0]?.id}>
                            <TabsList className="flex-col h-full [&>div]:w-full min-w-[200px]">
                                {tabs.map((tab, index) => (
                                    <DraggableTab
                                        key={tab.id}
                                        tab={tab}
                                        index={index}
                                        moveTab={moveTab}
                                    />
                                ))}
                            </TabsList>
                            {tabs.map((tab) => (
                                <TabsContent
                                    value={tab.id}
                                    key={tab.id}
                                    className="w-full mt-0 ml-4"
                                >
                                    <EditTabForm tab={tab} />
                                </TabsContent>
                            ))}
                        </Tabs>
                    ) : (
                        <NoContentMessage data={tabs} />
                    )}
                </div>
            </CardContent>
        </Card>
    );
}

export default TabListComponent;
