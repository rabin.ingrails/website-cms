import { HardDrive } from "lucide-react";

const NoContentMessage = ({ data }: { data: any }) => {
    if (!data || !data?.length)
        return (
            <div className="flex flex-col justify-center items-center h-full bg-gray-50 p-4 text-gray-500">
                <HardDrive className="h-12 w-12 " />
                <p>Not Added</p>
            </div>
        );

    return null;
};
export default NoContentMessage