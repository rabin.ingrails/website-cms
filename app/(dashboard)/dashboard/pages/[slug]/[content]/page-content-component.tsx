'use client';

import React, { Suspense, useState } from 'react';
import { Content, Gallery, Page } from '@prisma/client';
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from '@/components/ui/card';
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from '@/components/ui/dialog';

import {
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
    BreadcrumbList,
    BreadcrumbPage,
    BreadcrumbSeparator,
} from '@/components/ui/breadcrumb';
import {
    Table,
    TableBody,
    TableCaption,
    TableCell,
    TableHead,
    TableHeader,
    TableRow,
} from '@/components/ui/table';
import { format } from 'date-fns';

import { PlusIcon, PlusCircleIcon } from 'lucide-react';
import { GalleryContext } from '@/context/gallery-context';
import ContentUpdateForm from '@/components/forms/content-update.form';

function PageContentComponent({
    content,
    gallery,
}: {
    content: any;
    gallery: Gallery[] | undefined;
}) {
    return (
        <GalleryContext.Provider value={gallery}>
            <main className="">
                <div className=" pb-4 flex justify-between">
                    <div>
                        <h1 className="text-2xl font-bold capitalize">
                            {content?.title}
                        </h1>
                    </div>
                </div>
                <Suspense fallback={<div>Loading...</div>}>
                    <ContentUpdateForm content={content} />
                </Suspense>
            </main>
        </GalleryContext.Provider>
    );
}

export default PageContentComponent;
