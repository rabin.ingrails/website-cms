import AddPageForm from '@/components/forms/add-page-form';
import PageListTable from '@/components/tables/page-list-table';
import { getPageService } from '@/lib/api';

export default async function Page() {
    const pageList = await getPageService(false);

    return (
        <section className="">
            <div className="flex justify-between mb-10">
                <div>
                    <h1 className="text-2xl font-bold">Pages</h1>
                    <p className="text-secondary-foreground">
                        Following are list of pages
                    </p>
                </div>
                <AddPageForm formType='add-new-page' data={null} triggerText="Add New Page" modalTitle='Add New Page' />
            </div>

            <PageListTable pages={pageList} />
        </section>
    );
}
