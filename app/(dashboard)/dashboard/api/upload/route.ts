import { createPresignedPost } from '@aws-sdk/s3-presigned-post';
import { S3Client } from '@aws-sdk/client-s3';
import { v4 as uuidv4 } from 'uuid';

export async function POST(request: Request) {
  const { filename, contentType, projectKey } = await request.json();

  try {
    const client = new S3Client({
      region: process.env.S3_REGION,
      credentials: {
        accessKeyId: process.env.S3_ACCESS_KEY!,
        secretAccessKey: process.env.S3_SECRET_ACCESS_KEY!,
      },
    });

    // Extract the file extension from filename or contentType
    const fileExtension = filename.split('.').pop() || contentType.split('/')[1];
    const uniqueFileName = `${uuidv4()}.${fileExtension}`;

    const { url, fields } = await createPresignedPost(client, {
      Bucket: process.env.S3_BUCKET_NAME!,
      Key: `assets/2/veda-website-cms/gallery/${projectKey}/${uniqueFileName}`,
      Conditions: [
        ['content-length-range', 0, 10485760], // up to 10 MB
        ['starts-with', '$Content-Type', contentType],
      ],
      Fields: {
        acl: 'public-read',
        'Content-Type': contentType,
      },
      Expires: 600, // 10 minutes
    });

    return Response.json({ url, fields, fileUrl: `${url}/${fields.Key}` });
  } catch (error: any) {
    return Response.json({ error: error.message });
  }
}
