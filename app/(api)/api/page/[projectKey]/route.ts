import { listContentPerPageService } from '@/lib/api';
import { revalidateTag } from 'next/cache';

const CACHE_TAG = 'page-content';
const REVALIDATE_SECONDS = 10;
export async function GET(
    request: Request,
    { params }: { params: { projectKey: string } }
) {
    const projectKey = params.projectKey;
    
    const { searchParams } = new URL(request.url);
    //*NOTE: slug must start with / for resource request from client
    const pageSlug = searchParams.get('slug');

    // Revalidate the cache
    revalidateTag(CACHE_TAG);

    const response = new Response(
        JSON.stringify({
            data: await listContentPerPageService(projectKey, pageSlug || ''),
        }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Cache-Control': `s-maxage=${REVALIDATE_SECONDS}, stale-while-revalidate`,
            },
        }
    );

    return response;
}