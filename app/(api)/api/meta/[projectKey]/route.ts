import {  listMetaDataByProjectKeyService,  } from "@/lib/api"
import { revalidateTag } from "next/cache"
import { headers } from "next/headers"

// Define cache config
const CACHE_TAG = 'meta'
const REVALIDATE_SECONDS = 10

// API route handler
export async function GET(
    request: Request,
    { params }: { params: { projectKey: string } }
) {
    const projectKey = params.projectKey
    const headersList = headers()
    
    // Revalidate the cache
    revalidateTag(CACHE_TAG);

    // Add cache control headers
    const response = new Response(
        JSON.stringify({
            data: await listMetaDataByProjectKeyService(projectKey),
        }),
        {
            headers: {
                'Content-Type': 'application/json',
                'Cache-Control': `s-maxage=${REVALIDATE_SECONDS}, stale-while-revalidate`,
            },
        }
    )
    
    return response
}