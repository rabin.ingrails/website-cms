import SignInForm from '@/components/forms/sign-in-form';
import React from 'react';

function page() {
    return (
        <div className="min-h-screen w-full mt-[15vh]">
            <SignInForm></SignInForm>
        </div>
    );
}

export default page;
