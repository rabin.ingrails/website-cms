import SignUpForm from '@/components/forms/sign-up-form';
import React from 'react';

function page() {
    return (
        <div className="min-h-screen w-full mt-[15vh]">
            <SignUpForm></SignUpForm>
        </div>
    );
}

export default page;
