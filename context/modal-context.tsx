import { createContext } from 'react';
export interface ModalContextType {
    open: boolean;
    onClose: () => void;
}
export const ModalContext = createContext<ModalContextType | undefined>(undefined);
