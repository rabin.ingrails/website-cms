// import { PrismaClient } from '@prisma/client';
// const prisma = new PrismaClient();
// async function main() {

//     //* check if cjnepal@veda.com user exists
//     const userExists = await prisma.user.findFirst({
//         where: {
//             email: 'cjnepal@veda.com',
//         },
//         include: {
//             page: true,
//         },
//     });
//     if (userExists) {
//         //* delete user pages and content

//         await prisma.content.deleteMany({
//             where: {
//                 pageId: {
//                     in: userExists.page.map((p) => p.id),
//                 },
//             },
//         });
//         await prisma.page.deleteMany({
//             where: {
//                 userId: userExists.id,
//             },
//         });
//         //* delete user
//         await prisma.user.delete({
//             where: {
//                 id: userExists.id,
//             },
//         });
//     }
//     //*create user
//     const user = await prisma.user.create({
//         data: {
//             email: 'cjnepal@veda.com',
//             password:
//                 '$argon2id$v=19$m=65536,t=3,p=4$aLtwGV8LIRNArKz/1pgXIw$Hl/ZS+64Rn8OAaiAnWtGqQkIS0bKvcx02apVS0P7bk4',
//         },
//     });
//     //*add pages
//     const homepage = await prisma.page.create({
//         data: {
//             title: 'Home',
//             userId: user.id,
//             slug:'/',
//             content: {
//                 create: [
//                     {
//                         title: 'Welcome to the Himalayan Region of the Congregation Jesus',
//                         section: '1',
//                         desc: 'We are the sisters of the Congregation of Jesus, founded by Mary Ward. She was born in 1585 to a Catholic recusant family in North Yorkshire, England. Our Nepal Region is one of the 13 Provinces and Regions of the Congregation of Jesus. Mary Ward founded this International Congregation in 1609. We live in different communities worldwide but belong primarily to the apostolic body of the whole C.J. family, with its headquarters in Rome.',
//                         img: [],
//                     },
//                     {
//                         title: 'Who are we?',
//                         section: '2',
//                         desc: 'We are the sisters of the Congregation of Jesus, founded by Mary Ward, an English woman born in 1585 to a Catholic recusant family in North Yorkshire, England. Despite facing persecution, her family remained steadfast in their faith. At 15, Mary felt called to a religious life, marking the start of her extraordinary journey to discern God&apos;s will even in adversity. Initially, Mary joined the Poor Clares in St. Omer, seeking a strict contemplative life. However, she realized that life behind convent walls was not her true calling and returned to London in 1609. Her exemplary life inspired many young women to join her, and they engaged in apostolic activities, defying the persecution laws against Catholics in London. Later that same year, Mary discerned a deeper calling to a life "more to His glory". She left London with her companions and founded her first house in St. Omer in 1609. In 1611, during prayer, she received enlightenment with the words "take the same of the Society", which she understood to mean the Society of Jesus, founded by St. Ignatius of Loyola. Mary then devoted her life to establishing a congregation of women based on the Ignatian model—without cloister, governed by women, and sent anywhere in the world by the Pope.',
//                         img: [],
//                     },
//                     {
//                         title: 'Vision of the Congregation of Jesus in Nepal',
//                         section: '3',
//                         desc: '',
//                         img: [],
//                     },
//                     {
//                         title: 'Get in Touch!',
//                         section: '4',
//                         desc: '',
//                         img: [],
//                     },
//                 ],
//             },
//         },
//     });
//     const aboutpage = await prisma.page.create({
//         data: {
//             title: 'About Us',
//             userId: user.id,
//             slug:'/',
//             content: {
//                 create: [
//                     {
//                         title: 'About Us',
//                         section: '1',
//                         img: [],
//                         desc: '',
//                     },
//                     {
//                         title: 'Vision of the Congregation of Jesus in Nepal',
//                         section: '2',
//                         img: [],
//                         desc: "We the sisters of the Congregation of Jesus, following the Charism of our Foundress Venerable Mary Ward, engage in all apostolic activities, such as Education- formal and non -formal , Health, Women Empowerment, social work and all other good activities. We resolve to live in communities joyfully, living Jesus' Culture, respecting one another's uniqueness and commit ourselves fully to the mission of the Region.",
//                     },
//                     {
//                         title: 'Where are we?',
//                         section: '3',
//                         desc: '',
//                         img: [],
//                     },
//                 ],
//             },
//         },
//     });
// }
// main()
//     .then(async () => {
//         await prisma.$disconnect();
//     })
//     .catch(async (e) => {
//         console.error(e);
//         await prisma.$disconnect();
//         process.exit(1);
//     });

