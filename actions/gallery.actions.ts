'use server';

import { updateGalleryService } from "@/lib/api";
import { revalidatePath } from "next/cache";

export async function updateGalleryAction(formData: FormData): Promise<any> {
    const res = await updateGalleryService(
        JSON.parse(formData.get('gallery') as unknown as any)
    );
    revalidatePath('/dashboard/gallery')
    return {
        success: res.success,
        message: res.message,
    };

}