'use server';

import {
    addContentService,
    addPageService,
    createCardService,
    createRegularService,
    createTabService,
    deleteContentSectionService,
    removeCardService,
    removePageService,
    removeRegularService,
    removeTabService,
    updateCardOrderService,
    updateCardService,
    updateContentOrderService,
    updatePageService,
    updateRegularOrderService,
    updateRegularService,
    updateTabOrderService,
    updateTabService,
    updateTitleService,
} from '@/lib/api';
import { TabContent } from '@prisma/client';
import { revalidatePath, revalidateTag } from 'next/cache';

export async function addContentAction(
    {pageId, title, subTitle}: {pageId: string, title: string, subTitle: string},
): Promise<any> {

    const res = await addContentService({
        pageId: pageId,
        title: title,
        subTitle: subTitle,
    });

    revalidateDashboardPagesSlug();

    return {
        success: res.success,
        message: res.message,
        data: res.data,
    };
}

export async function addPageAction(
    bindValues: any,
    prevData: any,
    formData: FormData
): Promise<any> {
    const title = formData.get('title') as string;
    const slug = formData.get('slug') as string;
    const metaTags = bindValues.metaTags as string;
    const parentId = bindValues.parentId as string;
    const res = await addPageService({
        title,
        slug,
        metaTags,
        parentId,
    });
    revalidatePath('/dashboard/pages');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function updatePageAction(
    bindValues: any,
    prevData: any,
    formData: FormData
): Promise<any> {
    const id = bindValues.id as string;
    const metaTags = bindValues.metaTags as string;
    const parentId = bindValues.parentId as string;
    const title = formData.get('title') as string;
    const slug = formData.get('slug') as string;
    const res = await updatePageService({
        title,
        slug,
        id,
        metaTags,
        parentId,
    });
    revalidatePath('/dashboard/pages');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function createTabAction(formData: FormData): Promise<any> {
    const res = await createTabService(
        JSON.parse(formData.get('tab') as unknown as any)
    );
    revalidateDashboardPagesSlug();
    return {
        success: res.success,
        message: res.message,
    };
}

export async function updateTabAction(formData: FormData): Promise<any> {
    const res = await updateTabService({
        id: formData.get('id') as string,
        label: formData.get('label') as string,
        link: formData.get('link') as string,
        desc: formData.get('desc') as string,
        img: JSON.parse(formData.get('img') as string),
        contentId: formData.get('contentId') as string,
    });
    revalidatePath('/dashboard/pages/[slug]/[content]','page');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function createCardAction(formData: FormData): Promise<any> {
    const res = await createCardService(
        JSON.parse(formData.get('card') as unknown as any)
    );
    revalidatePath('/dashboard/pages/[slug]');

    return {
        success: res.success,
        message: res.message,
    };
}
export async function updateCardAction(formData: FormData): Promise<any> {
    const res = await updateCardService({
        id: formData.get('id') as string,
        title: formData.get('title') as string,
        subTitle: formData.get('subTitle') as string,
        link: formData.get('link') as string,
        desc: formData.get('desc') as string,
        img: JSON.parse(formData.get('img') as string),
        contentId: formData.get('contentId') as string,
    });
    revalidatePath('/dashboard/pages/[slug]');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function createRegularAction(formData: FormData): Promise<any> {
    const res = await createRegularService(
        JSON.parse(formData.get('regular') as unknown as any)
    );
    revalidatePath('/dashboard/pages/[slug]');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function updateRegularAction(formData: FormData): Promise<any> {
    const res = await updateRegularService({
        id: formData.get('id') as string,
        desc: formData.get('desc') as string,
        img: JSON.parse(formData.get('img') as string),
        contentId: formData.get('contentId') as string,
    });
    revalidatePath('/dashboard/pages/[slug]');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function updateTitleAction(formData: FormData): Promise<any> {
    const res = await updateTitleService({
        title: formData.get('title') as string,
        subTitle: formData.get('subTitle') as string,
        contentId: formData.get('contentId') as string,
    });
    revalidatePath('/dashboard/pages/[slug]');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function removeRegularAction(id: string): Promise<any> {
    const res = await removeRegularService(id);
    revalidatePath('/dashboard/pages/[slug]');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function removeTabAction(id: string): Promise<any> {
    console.log('🚀 ~ removeTabAction ~ id:', id);
    const res = await removeTabService(id);
    revalidatePath('/dashboard/pages/[slug]');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function removeCardAction(id: string): Promise<any> {
    const res = await removeCardService(id);
    revalidatePath('/dashboard/pages/[slug]');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function removePageAction(id: string): Promise<any> {
    const res = await removePageService(id);
    revalidatePath('/dashboard');

    return {
        success: res.success,
        message: res.message,
    };
}

export async function revalidateDashboardPagesSlug(): Promise<any> {
    revalidatePath('/dashboard/pages/[slug]', 'page');
}

export async function deleteContentSectionAction(
    contentId: string
): Promise<any> {
    const res = await deleteContentSectionService(contentId);
    revalidateDashboardPagesSlug()

    return {
        success: res.success,
        message: res.message,
    };
}
export async function revalidateFrontEndApis(): Promise<any> {
    revalidateTag('page-content');
    revalidateTag('meta');
}

export async function updateContentOrderAction(
    contentOrders: { id: string; order: number }[] | undefined
): Promise<any> {
    const res = await updateContentOrderService(contentOrders);
    revalidateDashboardPagesSlug();
    return {
        success: res.success,
        message: res.message,
    };
}

export async function updateTabOrderAction(
    tabOrders: { id: string; order: number }[] | undefined
): Promise<any> {
    const res = await updateTabOrderService(tabOrders);
    revalidatePath('/dashboard/pages/[slug]/[content]', 'page');    return {
        success: res.success,
        message: res.message,
    };
}
export async function updateCardOrderAction(
    cardOrders: { id: string; order: number }[] | undefined
): Promise<any> {
    const res = await updateCardOrderService(cardOrders);
    revalidatePath('/dashboard/pages/[slug]/[content]', 'page');
    return {
        success: res.success,
        message: res.message,
    };
}   

export async function updateRegularOrderAction(
    regularOrders: { id: string; order: number }[] | undefined
): Promise<any> {
    const res = await updateRegularOrderService(regularOrders);
    revalidatePath('/dashboard/pages/[slug]/[content]', 'page');
    return {
        success: res.success,
        message: res.message,
    };    
}