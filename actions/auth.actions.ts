'use server';
import { signInUserService, createUserService, Response, getUserInfoService } from '@/lib/api';
import { User } from '@prisma/client';
import { cookies } from 'next/headers';

export async function signInAction(
    prevState: any,
    formData: FormData
): Promise<any> {
    const email = formData.get('email') as string;
    const password = formData.get('password') as string;
    const res = await signInUserService({ email, password });
    return {
        success: res.success,
        message: res.message,
        res: res.res,
    };
}
export async function signUpAction(
    prevState: any,
    formData: FormData
): Promise<any> {
    const email = formData.get('email') as string;
    const password = formData.get('password') as string;
    const projectKey = formData.get('projectKey') as string;
    const res = await createUserService({ email, password, projectKey });
    return {
        success: res.success,
        message: res.message,
    };
}


export async function deleteCookie(name:string) {
    cookies().delete(name)
  }

export async function getUserInfo():Promise<{
    success: boolean,
    message: string,
    res?: Pick<User,'id' | 'email' | 'projectKey'> | null
}>{
    const res = await getUserInfoService()
    return {
        success: res.success,
        message: res.message,
        res: res?.res
    }
}