/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        // domains: ["s3-alpha-sig.figma.com"],
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'veda-app.s3.ap-south-1.amazonaws.com',
            },
        ],
        unoptimized: true,
    },

    async redirects() {
        return [
            {
                source: '/',
                destination: '/dashboard',
                permanent: false,
            },
        ];
    },
};
if(process.env.NEXT_PUBLIC_PREFIX){
    nextConfig.basePath = `/${process.env.NEXT_PUBLIC_PREFIX}`
}


export default nextConfig;
