import React, { useEffect } from "react";
import { Button, ButtonProps } from "../ui/button";
import { ModalContext, ModalContextType } from "@/context/modal-context";
import { X } from "lucide-react";

function ModalComponent({
  children,
  trigger,
  triggerVariant='outline',
}: {
  children: React.ReactNode | ((context: ModalContextType) => React.ReactNode);
  trigger: React.ReactNode;
  triggerVariant?: ButtonProps["variant"];
}) {
  const [open, setOpen] = React.useState(false);

  const onClose = () => {
    console.log("closed");
    setOpen(false);
  };

  const onOpen = () => {
    setOpen(true);
  };

  const renderChildren = () => {
    if (typeof children === "function") {
      return (children as (context: ModalContextType) => React.ReactNode)({
        open,
        onClose,
      });
    }
    return children;
  };

  return (
    <div>
      <Button variant={triggerVariant} onClick={onOpen}>
        {trigger}
      </Button>
      <ModalContext.Provider value={{ open, onClose }}>
        {open && (
          <div className="fixed inset-0 px-2 z-50 overflow-hidden bg-black/50 flex items-center justify-center !m-0 " >
            <div className="bg-white w-[70vw] h-[80vh] rounded-lg overflow-auto p-8 relative ">
              {renderChildren()}
              <button
                type="button"
                onClick={onClose}
                className="absolute top-2 right-2 z-50"
              >
                <X />
              </button>
            </div>
          </div>
        )}
      </ModalContext.Provider>
    </div>
  );
}

export default ModalComponent;
