import { cn } from '@/lib/utils';
import Image from 'next/image';
import React from 'react';

function Logo({
    className,
    size = 'lg',
}: {
    className?: string;
    size?: 'sm' | 'md' | 'lg';
}) {
    const sizeMap = {
        sm: 'w-6 h-6',
        md: 'w-16 h-16',
        lg: 'w-24 h-24',
    };
  
    return (
            <Image
                src="/logo.png"
                alt="logo"
                className={cn('object-contain', sizeMap[size], className)}
                width={80}
                height={80}
            />
    );
}

export default Logo;
