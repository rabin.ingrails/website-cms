'use client';
import React, { useState, useCallback, useEffect } from 'react';
import { Content, Gallery, Page } from '@prisma/client';
import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from '@/components/ui/dialog';
import {
    Table,
    TableBody,
    TableCaption,
    TableCell,
    TableHead,
    TableHeader,
    TableRow,
} from '@/components/ui/table';
import { format } from 'date-fns';
import { Button } from '../ui/button';
import { GripVertical, PlusCircleIcon } from 'lucide-react';
import { GalleryContext } from '@/context/gallery-context';
import Link from 'next/link';
import SectionContentAddForm from '../forms/section-content-add.form';
import { toast } from 'sonner';
import { updateContentOrderAction } from '@/actions/content.actions';
import { cn } from '@/lib/utils';

const DraggableTableRow = ({
    content,
    page,
    index,
    moveRow,
    className = '',
    handlePageOrderChange,
}: any) => {
    const [{ isDragging }, drag] = useDrag({
        type: 'table-row',
        item: { index },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    });

    const [, drop] = useDrop({
        accept: 'table-row',
        hover: (draggedItem: { index: number }, monitor) => {
            if (draggedItem.index === index) {
                return;
            }
            moveRow(draggedItem.index, index);
            draggedItem.index = index;
        },
        drop: (draggedItem: { index: number }) => {
            console.log('🚀 ~ draggedItem:', draggedItem);
        },
    });

    return (
        <TableRow
            ref={(node) => drag(drop(node)) as any}
            className={cn(className, isDragging ? 'opacity-50 bg-gray-50' : '')}
        >
            <TableCell>
                <div className="cursor-grab active:cursor-grabbing">
                    <GripVertical className="w-4 h-4 text-gray-400" />
                </div>
            </TableCell>
            <TableCell>{content.section || ''}</TableCell>
            <TableCell>
                {(content.title && content.title.substring(0, 50) + '...') ||
                    ''}
            </TableCell>
            <TableCell>
                {content.subTitle
                    ? content.subTitle.substring(0, 50) + '...'
                    : ''}
            </TableCell>
            <TableCell>
                {format(new Date(content.updatedAt), 'dd MMM yyyy')}
            </TableCell>
            <TableCell>
                <Link href={`/dashboard/pages/${page?.id}/${content.id}`}>
                    <Button variant="secondary" size="sm">
                        View
                    </Button>
                </Link>
            </TableCell>
        </TableRow>
    );
};

const PageTab = ({
    contents: initialContents,
    page,
    gallery,
}: {
    contents: Content[] | undefined;
    page: Page | undefined | null;
    gallery: Gallery[] | undefined;
}) => {
    const [contents, setContents] = useState(initialContents);
    const [openContentAddModal, setOpenContentAddModal] = useState(false);
    // useEffect(() => {
    //     setContents(initialContents);
    // }, [initialContents]);
    const handleUpdateOrder = async () => {
        //*call POST API
        console.log('contents', contents);

        try {
            const res = await updateContentOrderAction(
                contents?.map((i) => ({ id: i.id, order: i.section }))
            );
            if (res.success) {
                toast.success(res.message);
            } else {
                toast.error(res.message);
            }
        } catch (err: any) {
            console.log('🚀 ~ err ~ err:', err);
            toast.error(err.message);
        }
    };
    const moveRow = useCallback((dragIndex: number, hoverIndex: number) => {
        setContents((prevContents: any) => {
            if (!prevContents) return prevContents;

            const newContents = [...prevContents];
            const draggedRow = newContents[dragIndex];

            newContents.splice(dragIndex, 1);
            newContents.splice(hoverIndex, 0, draggedRow);

            // Update section numbers
            const updatedContents = newContents.map((content, index) => ({
                ...content,
                section: (index + 1).toString(),
            }));

            return updatedContents;
        });
    }, []);

    const handleModalClose = (id: string) => {
        setOpenContentAddModal(false);
    };

    return (
        <DndProvider backend={HTML5Backend}>
            <GalleryContext.Provider value={gallery}>
                <main className="min-h-screen">
                    <div className="pb-4 flex justify-between">
                        <div>
                            <h1 className="text-2xl font-bold capitalize">
                                {page?.title}
                            </h1>
                        </div>
                        {contents && contents?.length > 0 && (
                            <Dialog
                                open={openContentAddModal}
                                onOpenChange={setOpenContentAddModal}
                            >
                                <DialogTrigger asChild>
                                    <Button size="sm">Add Section</Button>
                                </DialogTrigger>
                                <DialogContent
                                    className="min-w-[60vw]"
                                    onPointerDownOutside={(e) =>
                                        e.preventDefault()
                                    }
                                >
                                    <DialogHeader>
                                        <DialogTitle>
                                            Add New Section
                                        </DialogTitle>
                                        <DialogDescription className="py-5">
                                            {!!page?.id && (
                                                <SectionContentAddForm
                                                    pageId={page.id}
                                                    onSuccess={handleModalClose}
                                                />
                                            )}
                                        </DialogDescription>
                                    </DialogHeader>
                                </DialogContent>
                            </Dialog>
                        )}
                    </div>
                    <div onDragEnd={handleUpdateOrder}>
                        <Table>
                            <TableCaption>
                                A list of all your sections. Drag to reorder.
                            </TableCaption>
                            <TableHeader>
                                <TableRow>
                                    <TableHead className="w-[50px]"></TableHead>
                                    <TableHead className="w-[100px]">
                                        Order
                                    </TableHead>
                                    <TableHead className="w-[400px]">
                                        Title
                                    </TableHead>
                                    <TableHead>Sub Title</TableHead>
                                    <TableHead>Last Updated</TableHead>
                                    <TableHead></TableHead>
                                </TableRow>
                            </TableHeader>
                            <TableBody>
                                {contents?.map((content, index) => (
                                    <DraggableTableRow
                                        key={content.id}
                                        content={content}
                                        page={page}
                                        index={index}
                                        moveRow={moveRow}
                                    />
                                ))}
                            </TableBody>
                        </Table>
                    </div>

                    <div>
                        {!contents ||
                            (contents?.length === 0 && (
                                <Dialog
                                    open={openContentAddModal}
                                    onOpenChange={setOpenContentAddModal}
                                >
                                    <DialogTrigger className="w-full">
                                        <div className="flex flex-col items-center gap-2 justify-center border rounded-lg bg-gray-50 p-10 text-gray-500 group w-full">
                                            <PlusCircleIcon className="w-10 h-10 group-hover:scale-105 group-hover:text-black" />
                                            <p className="group-hover:text-black">
                                                Add Section
                                            </p>
                                        </div>
                                    </DialogTrigger>
                                    <DialogContent className="min-w-[60vw]">
                                        <DialogHeader>
                                            <DialogTitle>
                                                Add New Section
                                            </DialogTitle>
                                            <DialogDescription className="py-5">
                                                {!!page?.id && (
                                                    <SectionContentAddForm
                                                        pageId={page.id}
                                                        onSuccess={
                                                            handleModalClose
                                                        }
                                                    />
                                                )}
                                            </DialogDescription>
                                        </DialogHeader>
                                    </DialogContent>
                                </Dialog>
                            ))}
                    </div>
                </main>
            </GalleryContext.Provider>
        </DndProvider>
    );
};

export default PageTab;
