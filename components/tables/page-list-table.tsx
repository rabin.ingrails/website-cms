'use client';

import { Page } from '@prisma/client';
import AddPageForm from '../forms/add-page-form';
import Link from 'next/link';
import { Button } from '../ui/button';
import { DeleteIcon, TrashIcon } from 'lucide-react';
import { removePageAction } from '@/actions/content.actions';

import {
    AlertDialog,
    AlertDialogAction,
    AlertDialogCancel,
    AlertDialogContent,
    AlertDialogDescription,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogTitle,
    AlertDialogTrigger,
} from '@/components/ui/alert-dialog';
import { toast } from 'sonner';
import { PageWithSubPages } from '@/types';
import {
    Table,
    TableBody,
    TableCaption,
    TableCell,
    TableHead,
    TableHeader,
    TableRow,
} from '@/components/ui/table';
const GenerateTableRows = ({
    page,
    hasChild = false,
    isChild = false,
}: {
    page: Page;
    hasChild?: boolean;
    isChild?: boolean;
}) => {
    const handlePageDelete = async (id: string) => {
        const res = await removePageAction(id);
        if (!res?.success) return toast.error(res?.message);
        toast.success(res?.message);
    };

    return (
        <div className="grid grid-cols-3 items-center ">
            <p className=" col-span-1 ">{page.title}</p>
            <p className=" col-span-1">{page.slug}</p>

            <div className="font-medium col-span-1">
                <div className="flex gap-2 justify-end">
                    <AddPageForm
                        formType="edit-page"
                        data={page}
                        triggerText="Edit Page"
                        modalTitle="Edit Page"
                    />
                    {/* {!isChild && <AddPageForm formType='add-sub-page'  triggerText='Add Sub Page' modalTitle='Add Sub Pages' parentId={page.id} />} */}

                    <Link href={`/dashboard/pages/${page.id}`}>
                        <Button size={'sm'} variant={'secondary'}>
                            {' '}
                            Add Content
                        </Button>
                    </Link>

                    {!hasChild && (
                        <AlertDialog>
                            <AlertDialogTrigger>
                                <Button size={'icon'} variant={'secondary'}>
                                    <TrashIcon className="w-4 h-4 text-red-500" />
                                </Button>
                            </AlertDialogTrigger>
                            <AlertDialogContent>
                                <AlertDialogHeader>
                                    <AlertDialogTitle>
                                        Are you sure you want to delete this
                                        page?
                                    </AlertDialogTitle>
                                    <AlertDialogDescription>
                                        This action cannot be undone.
                                    </AlertDialogDescription>
                                </AlertDialogHeader>
                                <AlertDialogFooter>
                                    <AlertDialogCancel>
                                        Cancel
                                    </AlertDialogCancel>
                                    <AlertDialogAction
                                        className="bg-red-500 hover:bg-red-600"
                                        onClick={() =>
                                            handlePageDelete(page.id)
                                        }
                                    >
                                        Delete
                                    </AlertDialogAction>
                                </AlertDialogFooter>
                            </AlertDialogContent>
                        </AlertDialog>
                    )}
                </div>
            </div>
        </div>
    );
};
// function PageListTable({ pages }: { pages: PageWithSubPages[] }) {
//     return (
//         <div className=" space-y-2  p-2 text-sm ">
//             <div className="grid grid-cols-3 p-4 text-gray-600 ">
//                 <p className=" font-medium col-span-1">Title</p>
//                 <p className=" font-medium col-span-1">Slug</p>
//                 <p className=" font-medium col-span-1 place-self-end">
//                     Actions
//                 </p>
//             </div>
//             {pages
//                 ?.filter((i) => !i.parentId)
//                 .map((item) => {
//                     if (!item.subPages.length)
//                         return (
//                             <div className=" p-4 border-b" key={item.id}>
//                                 <GenerateTableRows
//                                     page={item}
//                                 />
//                             </div>
//                         );
//                     return (
//                         <div key={item.id} className="p-4 border-b ">
//                             <GenerateTableRows
//                                 key={item.id}
//                                 page={item}
//                                 hasChild
//                             />
//                             <div className="ml-8  rounded-lg  space-y-2 mt-4 pl-4 py-2 bg-white">
//                                 <p className="text-sm border-b pb-2 ">Sub Pages</p>
//                                 {item.subPages?.map((j) => (
//                                     <div className="py-2 border-b" key={j.id}>
//                                         <GenerateTableRows
//                                             key={j.id}
//                                             page={j}
//                                             isChild
//                                         />
//                                     </div>
//                                 ))}
//                             </div>
//                         </div>
//                     );
//                 })}
//         </div>
//     );
// }

function PageListTable({ pages }: { pages: PageWithSubPages[] }) {
    const handlePageDelete = async (id: string) => {
        const res = await removePageAction(id);
        if (!res?.success) return toast.error(res?.message);
        toast.success(res?.message);
    };

    return (
        <Table>
            <TableHeader>
                <TableRow>
                    <TableHead className="w-[400px]">Title</TableHead>
                    <TableHead>Slug</TableHead>
                    <TableHead className="text-right">Actions</TableHead>
                </TableRow>
            </TableHeader>
            <TableBody>
                {pages.map((item) => (
                    <TableRow key={item.id}>
                        <TableCell className="font-medium">
                            {item.title}
                        </TableCell>
                        <TableCell>{item.slug}</TableCell>
                        <TableCell className="flex gap-2 justify-end items-center">
                            <AddPageForm
                                formType="edit-page"
                                data={item}
                                triggerText="Edit Page"
                                modalTitle="Edit Page"
                            />

                            <Link href={`/dashboard/pages/${item.id}`}>
                                <Button size={'sm'} variant={'secondary'}>
                                    Add Content
                                </Button>
                            </Link>
                            <AlertDialog>
                            <AlertDialogTrigger>
                                <Button size={'icon'} variant={'secondary'}>
                                    <TrashIcon className="w-4 h-4 text-red-500" />
                                </Button>
                            </AlertDialogTrigger>
                            <AlertDialogContent>
                                <AlertDialogHeader>
                                    <AlertDialogTitle>
                                        Are you sure you want to delete this
                                        page?
                                    </AlertDialogTitle>
                                    <AlertDialogDescription>
                                        This action cannot be undone.
                                    </AlertDialogDescription>
                                </AlertDialogHeader>
                                <AlertDialogFooter>
                                    <AlertDialogCancel>
                                        Cancel
                                    </AlertDialogCancel>
                                    <AlertDialogAction
                                        className="bg-red-500 hover:bg-red-600"
                                        onClick={() =>
                                            handlePageDelete(item.id)
                                        }
                                    >
                                        Delete
                                    </AlertDialogAction>
                                </AlertDialogFooter>
                            </AlertDialogContent>
                        </AlertDialog>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>

         
        </Table>
    );
}

export default PageListTable;
