'use client';
import React, { useContext, useState } from 'react';
import { Input } from '../ui/input';
import dynamic from 'next/dynamic';
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from '../ui/form';
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from '@/components/ui/dialog';

import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { Button } from '../ui/button';
import { MinusCircledIcon, PlusCircledIcon } from '@radix-ui/react-icons';
import {
    createTabAction,
    removeCardAction,
    removeTabAction,
    updateCardAction,
    updateTabAction,
} from '@/actions/content.actions';
import { CardContent, Content, TabContent } from '@prisma/client';
import { removeTabService } from '@/lib/api';
import { toast } from 'sonner';
import { Badge } from '../ui/badge';
import { GalleryViewerSmall } from '../gallery/gallery-viewer';
import { GalleryContext } from '@/context/gallery-context';
import Image from 'next/image';
import ModalComponent from '../modal/modal-component';
import { ModalContext } from '@/context/modal-context';
const CustomEditor = dynamic(() => import('../editor/custom-editor'), {
    ssr: false,
});

function EditCardForm({ card }: { card: CardContent }) {
    const galleryImages = useContext(GalleryContext);

    const form = useForm({
        defaultValues: card,
    });

    const handleCardRemove = async () => {
        const res = await removeCardAction(card.id);
        toast.success(res?.message);
    };

    const onSubmit = async (data: any, onClose: () => void) => {
        const formData = new FormData();
        formData.append('id', card.id);
        formData.append('title', data.title);
        formData.append('subTitle', data.subTitle);
        formData.append('link', data.link);
        formData.append('desc', data.desc);
        formData.append('img', JSON.stringify(data.img));
        formData.append('contentId', card.contentId);
        const res = await updateCardAction(formData);
        toast.success(res?.message);
        onClose();
    };

    return (
        <>
            <div className="">
                <ModalComponent trigger="Edit">
                    {({ onClose }) => (
                        <Form {...form}>
                            <form
                                onSubmit={form.handleSubmit((data) =>
                                    onSubmit(data, onClose)
                                )}
                            >
                                <div className="space-y-4 ">
                                    <div className="w-full flex items-center gap-5">
                                        <FormField
                                            control={form.control}
                                            name={'title'}
                                            render={({ field }) => (
                                                <FormItem>
                                                    <FormLabel>Title</FormLabel>
                                                    <FormControl>
                                                        <Input
                                                            placeholder="Title"
                                                            {...field}
                                                        />
                                                    </FormControl>
                                                    <FormMessage />
                                                </FormItem>
                                            )}
                                        />
                                        <FormField
                                            control={form.control}
                                            name={`subTitle`}
                                            render={({ field }) => (
                                                <FormItem>
                                                    <FormLabel>
                                                        SubTitle
                                                    </FormLabel>
                                                    <FormControl>
                                                        <Input
                                                            placeholder="Sub Title"
                                                            {...field}
                                                        />
                                                    </FormControl>
                                                    <FormMessage />
                                                </FormItem>
                                            )}
                                        />
                                        <FormField
                                            control={form.control}
                                            name={`link`}
                                            render={({ field }) => (
                                                <FormItem>
                                                    <FormLabel>Link</FormLabel>
                                                    <FormControl>
                                                        <Input
                                                            placeholder="Link"
                                                            {...field}
                                                        />
                                                    </FormControl>
                                                    <FormMessage />
                                                </FormItem>
                                            )}
                                        />
                                    </div>
                                    <Controller
                                        control={form.control}
                                        name={`desc`}
                                        render={({ field }) => (
                                            <CustomEditor
                                                className=" w-full h-[300px] "
                                                value={field.value}
                                                onChange={field.onChange as any}
                                            />
                                        )}
                                    />
                                </div>
                                <div className="space-y-4 col-span-5">
                                    <div className="flex justify-between">
                                        <p>Add Gallery Images</p>
                                        <Badge variant="default">
                                            {form.watch(`img`)?.length} Selected
                                        </Badge>
                                    </div>
                                    <div className="max-h-[500px] overflow-auto ">
                                        <Controller
                                            control={form.control}
                                            name={`img`}
                                            render={({ field }) => (
                                                <GalleryViewerSmall
                                                    gallery={galleryImages}
                                                    onSelect={field.onChange}
                                                    value={field.value as any}
                                                    name={`img`}
                                                />
                                            )}
                                        />
                                    </div>
                                </div>

                                <div className="flex justify-between mt-20">
                                    <Button
                                        type="button"
                                        onClick={() => handleCardRemove()}
                                        variant={'destructive'}
                                    >
                                        {/* <MinusCircledIcon className="" /> */}
                                        Delete
                                    </Button>
                                    <Button type="submit">Save</Button>
                                </div>
                            </form>
                        </Form>
                    )}
                </ModalComponent>
            </div>
        </>
    );
}

export default EditCardForm;
