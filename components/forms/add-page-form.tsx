'use client';
import { Input } from '@/components/ui/input';
import { SubmitButton } from '../ui/submit-button';
import { useFormState } from 'react-dom';
import { toast } from 'sonner';
import { useEffect, useState } from 'react';
import { Label } from '@/components/ui/label';

import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from '@/components/ui/dialog';

import { Textarea } from '../ui/textarea';
import { Content, Page } from '@prisma/client';
import dynamic from 'next/dynamic';
import { addPageAction, updatePageAction } from '@/actions/content.actions';
import { Button } from '../ui/button';
import { cn } from '@/lib/utils';
import { RadioGroup, RadioGroupItem } from '@/components/ui/radio-group';

const CustomEditor = dynamic(() => import('../editor/custom-editor'), {
    ssr: false,
});
import { revalidatePath } from 'next/cache';
import { PageWithSubPages } from '@/types';

const initialState = {
    success: null,
    message: null,
};
//*Three possible states
//*Add new page -> no data
//*Edit page -> requries data
//*Add sub page -->requries parentId but doesnt need data

function AddPageForm({
    formType = 'add-new-page',
    data = null,
    parentId = null,
    triggerText = '',
    modalTitle = '',
}: {
    formType?: 'add-new-page' | 'edit-page' | 'add-sub-page';
    data?: Page | null;
    parentId?: string | null;
    triggerText?: string;
    modalTitle?: string;
}) {
    const [metaTags, setMetaTags] = useState(data?.metaTags || '');

    const updatePageActionWithId = updatePageAction.bind(null, {
        id: data?.id,
        metaTags: metaTags,
        parentId: data?.parentId, //leave as it is,
    });

    const addPageActionWithMetaAndParent = addPageAction.bind(null, {
        parentId: parentId,
        metaTags: metaTags,
    });

    const [state, formAction] = useFormState(
        formType === 'edit-page'
            ? updatePageActionWithId
            : addPageActionWithMetaAndParent,
        initialState
    );

    useEffect(() => {
        if (state?.success === true) {
            toast.success(state.message);
        } else if (state?.success === false) {
            toast.error(state.message);
        }
    }, [state?.success, state?.message]);

    return (
        <Dialog>
            <DialogTrigger>
                <Button
                    size={'sm'}
                    variant={
                        formType === 'add-new-page' ? 'default' : 'secondary'
                    }
                >
                    {triggerText}
                </Button>
            </DialogTrigger>
            <DialogContent className="lg:min-w-[800px]  ">
                <DialogHeader>
                    <DialogTitle>{modalTitle}</DialogTitle>
                    <DialogDescription>
                        <form action={formAction} className="space-y-6 mt-4">
                            <div className="space-y-2">
                                <Label>Page Title</Label>
                                <Input
                                    name="title"
                                    required
                                    defaultValue={
                                        formType === 'edit-page' && data
                                            ? data.title
                                            : ''
                                    }
                                    placeholder="Enter Page Title"
                                />
                            </div>
                            <div className="space-y-2">
                                <Label>Page Slug</Label>
                                <Input
                                    defaultValue={
                                        formType === 'edit-page' && data
                                            ? data.slug
                                            : ''
                                    }
                                    name="slug"
                                    required
                                    placeholder="Enter Page Title"
                                />
                            </div>

                            <div className="space-y-2  block ">
                                <Label>MetaTags</Label>
                                {/* <CustomEditor
                                            className="h-80"
                                            value={metaTags}
                                            onChange={setMetaTags}
                                        /> */}
                                <Textarea
                                    onChange={(e) =>
                                        setMetaTags(e.target.value)
                                    }
                                    value={metaTags}
                                    rows={10}
                                ></Textarea>
                            </div>

                            <DialogTrigger >
                                <SubmitButton className="w-auto " type="submit">
                                    Save
                                </SubmitButton>
                            </DialogTrigger>
                        </form>
                    </DialogDescription>
                </DialogHeader>
            </DialogContent>
        </Dialog>
    );
}

export default AddPageForm;
