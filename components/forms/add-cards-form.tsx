'use client';
import React, { useContext, useState } from 'react';
import { Input } from '../ui/input';
import dynamic from 'next/dynamic';
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from '../ui/form';
import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { Button } from '../ui/button';
import { MinusCircledIcon, PlusCircledIcon } from '@radix-ui/react-icons';
import { createCardAction } from '@/actions/content.actions';
import { toast } from 'sonner';
import { DialogTrigger } from '../ui/dialog';
import { GalleryContext } from '@/context/gallery-context';
import { Badge } from '../ui/badge';
import { GalleryViewerSmall } from '../gallery/gallery-viewer';
import { ModalContext } from '@/context/modal-context';
const CustomEditor = dynamic(() => import('../editor/custom-editor'), {
    ssr: false,
});

const initialState = {
    title: '',
    subTitle: '',
    desc: '',
    link: '',
    img: [],
};
function AddCardsForm({ contentId }: { contentId: string }) {
    const galleryImages = useContext(GalleryContext);
    const modalTrigger = useContext(ModalContext);

    const form = useForm({
        defaultValues: {
            card: [initialState],
        },
    });

    const { fields, append, prepend, remove, swap, move, insert } =
        useFieldArray({
            control: form.control,
            name: 'card',
        });

    const onSubmit = async (data: any) => {
        const formData = new FormData();
        formData.append(
            'card',
            JSON.stringify(
                data?.card?.map((card: any) => ({
                    ...card,
                    img: card?.img,
                    contentId,
                }))
            )
        );
        const res = await createCardAction(formData);
        toast.success(res?.message);
        modalTrigger?.onClose();
    };
    return (
        <>
            <div className="">
                <h2 className="text-xl font-bold"> Add Cards</h2>
                <p className="italic text-end">
                    *All Changes will only reflect after save
                </p>
            </div>
            <Form {...form}>
                <form
                    onSubmit={form.handleSubmit(onSubmit)}
                    className="space-y-10 py-6"
                >
                    {fields.map((field, index) => (
                        <div
                            key={field.id}
                            className="grid gap-8 mb-12 grid-cols-12 pb-8 border-b rounded-md"
                        >
                            <div className="bg-gray-50 col-span-12 flex justify-between items-center p-2">
                                <h1 className="bg-black text-white font-bold text-xl h-8 w-8 flex justify-center items-center rounded-full">
                                    {index + 1}
                                </h1>
                                <Button
                                    size={'sm'}
                                    type="button"
                                    onClick={() => remove(index)}
                                    variant={'destructive'}
                                >
                                    <MinusCircledIcon className="mr-2" />
                                    Remove Card
                                </Button>
                            </div>
                            <div className="col-span-8">
                                <div className=" w-full mb-6 flex gap-20">
                                    <FormField
                                        control={form.control}
                                        name={`card.${index}.title`}
                                        render={({ field }) => (
                                            <FormItem>
                                                <FormLabel>Title</FormLabel>
                                                <FormControl>
                                                    <Input
                                                        placeholder="Title"
                                                        {...field}
                                                    />
                                                </FormControl>
                                                {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                    <FormField
                                        control={form.control}
                                        name={`card.${index}.subTitle`}
                                        render={({ field }) => (
                                            <FormItem>
                                                <FormLabel>Sub Title</FormLabel>
                                                <FormControl>
                                                    <Input
                                                        placeholder="Sub Title"
                                                        {...field}
                                                    />
                                                </FormControl>
                                                {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                    <FormField
                                        control={form.control}
                                        name={`card.${index}.link`}
                                        render={({ field }) => (
                                            <FormItem>
                                                <FormLabel>Link</FormLabel>
                                                <FormControl>
                                                    <Input
                                                        placeholder="Link"
                                                        {...field}
                                                    />
                                                </FormControl>
                                                {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                    {/* <FormField
                                    control={form.control}
                                    name={`card.${index}.img`}
                                    render={({ field }) => (
                                        <FormItem>
                                            <FormLabel>Img</FormLabel>
                                            <FormControl>
                                                <Input
                                                    placeholder="Img"
                                                    {...field}
                                                />
                                            </FormControl>
                                   
                                            <FormMessage />
                                        </FormItem>
                                    )}
                                /> */}
                                </div>
                                <Controller
                                    control={form.control}
                                    name={`card.${index}.desc`}
                                    render={({ field }) => (
                                        <>
                                            <CustomEditor
                                                className=" w-full max-h-full mb-6"
                                                value={field.value}
                                                onChange={field.onChange as any}
                                            />
                                        </>
                                    )}
                                />{' '}
                            </div>

                            <div className="space-y-4 col-span-4">
                                <div className="flex justify-between">
                                    <FormLabel>Add Gallery Images</FormLabel>
                                    <Badge variant="default">
                                        {
                                            form.watch(`card.${index}.img`)
                                                ?.length
                                        }{' '}
                                        Selected
                                    </Badge>
                                </div>
                                <Controller
                                    control={form.control}
                                    name={`card.${index}.img`}
                                    render={({ field }) => (
                                        <GalleryViewerSmall
                                            gallery={galleryImages}
                                            onSelect={field.onChange}
                                            value={field.value as any}
                                            name={`card.${index}.img`}
                                        />
                                    )}
                                />
                            </div>
                        </div>
                    ))}
                    <div className="flex justify-end gap-4 items-center">
                        <Button
                            type="button"
                            variant={'outline'}
                            onClick={() => append(initialState)}
                        >
                            <PlusCircledIcon className="mr-2" />
                            Add Cards{' '}
                        </Button>
                        {/* <DialogTrigger asChild> */}
                        <Button type="submit">Save</Button>
                        {/* </DialogTrigger> */}
                    </div>
                </form>
            </Form>
        </>
    );
}

export default AddCardsForm;
