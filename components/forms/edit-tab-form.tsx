'use client';
import React, { useContext, useState } from 'react';
import { Input } from '../ui/input';
import dynamic from 'next/dynamic';
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from '../ui/form';
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from '@/components/ui/dialog';

import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { Button } from '../ui/button';
import { MinusCircledIcon, PlusCircledIcon } from '@radix-ui/react-icons';
import {
    createTabAction,
    removeTabAction,
    updateTabAction,
} from '@/actions/content.actions';
import { Content, TabContent } from '@prisma/client';
import { removeTabService } from '@/lib/api';
import { toast } from 'sonner';
import Image from 'next/image';
import { Badge } from '../ui/badge';
import { GalleryViewerSmall } from '../gallery/gallery-viewer';
import { GalleryContext } from '@/context/gallery-context';
import ModalComponent from '../modal/modal-component';

const CustomEditor = dynamic(() => import('../editor/custom-editor'), {
    ssr: false,
});

const initialState = {
    label: '',
    link: '',
    desc: '',
    img: [],
    tabId: '',
};
function EditTabForm({ tab }: { tab: TabContent }) {
    console.log('🚀 ~ EditTabForm ~ tab:', tab);
    const galleryImages = useContext(GalleryContext);

    const form = useForm({
        defaultValues: tab,
    });

    const handleTabRemove = async () => {
        const res = await removeTabAction(tab.id);
        toast.success(res?.message);
    };

    const onSubmit = async (data: any, onClose: () => void) => {
        const formData = new FormData();
        formData.append('id', tab.id);
        formData.append('label', data.label);
        formData.append('link', data.link);
        formData.append('desc', data.desc);
        formData.append('img', JSON.stringify(data.img));
        formData.append('contentId', tab.contentId);
        const res = await updateTabAction(formData);
        toast.success(res?.message);
        onClose();
    };

    return (
        <>
            <div className="w-full  bg-slate-100 rounded-lg p-4  relative">
                <div className="absolute top-2 right-2">
                    <ModalComponent trigger="Edit">
                        {({ onClose }) => (
                            <Form {...form}>
                                <form
                                    onSubmit={form.handleSubmit((data) =>
                                        onSubmit(data, onClose)
                                    )}
                                >
                                    <div className="space-y-4 ">
                                        <div className="w-full flex items-center gap-5">
                                            <FormField
                                                control={form.control}
                                                name={'label'}
                                                render={({ field }) => (
                                                    <FormItem>
                                                        <FormLabel>
                                                            Label
                                                        </FormLabel>
                                                        <FormControl>
                                                            <Input
                                                                placeholder="Label"
                                                                {...field}
                                                            />
                                                        </FormControl>
                                                        {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                                        <FormMessage />
                                                    </FormItem>
                                                )}
                                            />
                                            <FormField
                                                control={form.control}
                                                name={`link`}
                                                render={({ field }) => (
                                                    <FormItem>
                                                        <FormLabel>
                                                            Link
                                                        </FormLabel>
                                                        <FormControl>
                                                            <Input
                                                                placeholder="Link"
                                                                {...field}
                                                            />
                                                        </FormControl>
                                                        {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                                        <FormMessage />
                                                    </FormItem>
                                                )}
                                            />
                                        </div>
                                        <Controller
                                            control={form.control}
                                            name={`desc`}
                                            render={({ field }) => (
                                                <CustomEditor
                                                    className=" w-full h-[300px] "
                                                    value={field.value}
                                                    onChange={
                                                        field.onChange as any
                                                    }
                                                />
                                            )}
                                        />
                                        <div className="space-y-4 col-span-5">
                                            <div className="flex justify-between">
                                                <p>Add Gallery Images</p>
                                                <Badge variant="default">
                                                    {form.watch(`img`)?.length}{' '}
                                                    Selected
                                                </Badge>
                                            </div>
                                            <div className="max-h-[500px] overflow-auto ">
                                                <Controller
                                                    control={form.control}
                                                    name={`img`}
                                                    render={({ field }) => (
                                                        <GalleryViewerSmall
                                                            gallery={
                                                                galleryImages
                                                            }
                                                            onSelect={
                                                                field.onChange
                                                            }
                                                            value={
                                                                field.value as any
                                                            }
                                                            name={`img`}
                                                        />
                                                    )}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="flex justify-between mt-20">
                                        <Button
                                            type="button"
                                            onClick={() => handleTabRemove()}
                                            variant={'destructive'}
                                        >
                                            {/* <MinusCircledIcon className="" /> */}
                                            Delete
                                        </Button>
                                        <Button type="submit">Save</Button>
                                    </div>
                                </form>
                            </Form>
                        )}
                    </ModalComponent>
                </div>

                <div className="">
                    <small>Link</small>
                    <p>{tab?.link}</p>

                    <div className="my-4 ">
                        <small className="">Tab Content</small>

                        <div className=" max-h-[500px] overflow-auto p-2">
                            <div
                                className="wysiwyg-preview"
                                dangerouslySetInnerHTML={{
                                    __html: tab?.desc,
                                }}
                            ></div>
                        </div>
                    </div>
                    <small className="">Images associated with this tab</small>

                    <div className="flex gap-4 p-2 overflow-auto">
                        {tab?.img?.map((i: string) => (
                            <Image
                                src={i}
                                alt={i}
                                width={200}
                                height={200}
                                key={i}
                                className="object-cover border-2 h-[150px]"
                            />
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
}

export default EditTabForm;
