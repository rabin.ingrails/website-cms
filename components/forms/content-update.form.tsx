'use client';
import {
    AlertDialog,
    AlertDialogAction,
    AlertDialogCancel,
    AlertDialogContent,
    AlertDialogDescription,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogTitle,
    AlertDialogTrigger,
} from '@/components/ui/alert-dialog';
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from '@/components/ui/card';

import {
    CardContent as CardContentType,
    Content,
    RegularContent,
    TabContent,
} from '@prisma/client';
import { Button } from '../ui/button';
import AddTabsForm from './add-tabs-form';
import AddCardsForm from './add-cards-form';
import AddRegularForm from './add-regular-form';

import EditTabForm from './edit-tab-form';
import EditCardForm from './edit-card-form';
import EditRegularForm from './edit-regular-form';
import EditTitleForm from './edit-title-form';
import { HardDrive } from 'lucide-react';
import { Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs';
import Image from 'next/image';

import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselNext,
    CarouselPrevious,
} from '@/components/ui/carousel';
import ModalComponent from '../modal/modal-component';
import { useState } from 'react';
import { deleteContentSectionAction } from '@/actions/content.actions';
import { useRouter } from 'next/navigation';
import { toast } from 'sonner';
import NoContentMessage from '@/app/(dashboard)/dashboard/pages/[slug]/[content]/no-content-message';
import TabListComponent from '@/app/(dashboard)/dashboard/pages/[slug]/[content]/tab-list-component';
import CardListComponent from '@/app/(dashboard)/dashboard/pages/[slug]/[content]/card-list-component';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

function ContentUpdateForm({ content }: { content: any }) {
    const router = useRouter();
    const deleteContent = async () => {
        const res = await deleteContentSectionAction(content?.id);
        if (res.success) {
            router.push('/dashboard/pages/' + content?.pageId);
            toast.success(res.message);
        }
    };
    return (
        // <form action={formAction}>
        <div className="space-y-4 ">
            <div className="flex justify-end gap-4">
                <ModalComponent trigger={'Add Tabs'} triggerVariant={'default'}>
                    <AddTabsForm contentId={content?.id} />
                </ModalComponent>
                <ModalComponent
                    trigger={'Add Cards'}
                    triggerVariant={'default'}
                >
                    <AddCardsForm contentId={content?.id} />
                </ModalComponent>
                <ModalComponent
                    trigger={'Add Descriptions'}
                    triggerVariant={'default'}
                >
                    <AddRegularForm contentId={content?.id} />
                </ModalComponent>
            </div>
            <Card>
                <CardHeader>
                    <CardTitle>Title</CardTitle>
                </CardHeader>
                <CardContent>
                    <EditTitleForm
                        title={content?.title}
                        subTitle={content?.subTitle}
                        contentId={content?.id}
                    />
                </CardContent>
            </Card>
            <DndProvider backend={HTML5Backend}>
                {content?.tab?.length > 0 && (
                    <TabListComponent tabs={content?.tab} />
                )}
                {content?.card?.length > 0 && (
                    <CardListComponent cards={content?.card} />
                )}
            </DndProvider>
            {content?.regular?.length > 0 && (
                <Card>
                    <CardHeader>
                        <CardTitle className="flex justify-between items-center">
                            Descriptions
                        </CardTitle>
                    </CardHeader>
                    <CardContent>
                        <div className="space-y-4">
                            {content?.regular?.map((i: RegularContent) => (
                                <EditRegularForm regular={i} key={i.id} />
                            ))}
                        </div>
                    </CardContent>
                </Card>
            )}

            <AlertDialog>
                <div className="flex flex-col items-end ">
                    <p className="text-sm text-red-500 mb-2">
                        *Delete this content from the page
                    </p>
                    <AlertDialogTrigger asChild>
                        <Button variant="destructive">Delete Content</Button>
                    </AlertDialogTrigger>
                </div>
                <AlertDialogContent>
                    <AlertDialogHeader>
                        <AlertDialogTitle>
                            Are you absolutely sure?
                        </AlertDialogTitle>
                        <AlertDialogDescription>
                            This action will delete this content from the page.
                        </AlertDialogDescription>
                    </AlertDialogHeader>
                    <AlertDialogFooter>
                        <AlertDialogCancel>Cancel</AlertDialogCancel>
                        <AlertDialogAction onClick={deleteContent}>
                            Delete
                        </AlertDialogAction>
                    </AlertDialogFooter>
                </AlertDialogContent>
            </AlertDialog>
        </div>
    );
}

export default ContentUpdateForm;
