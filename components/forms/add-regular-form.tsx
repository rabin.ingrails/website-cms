'use client';
import React, { useContext, useState } from 'react';
import { Input } from '../ui/input';
import dynamic from 'next/dynamic';
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from '../ui/form';
import { Badge } from '@/components/ui/badge';

import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { Button } from '../ui/button';
import { MinusCircledIcon, PlusCircledIcon } from '@radix-ui/react-icons';
import { createRegularAction } from '@/actions/content.actions';
import { toast } from 'sonner';
import { GalleryContext } from '@/context/gallery-context';
import { GalleryViewerSmall } from '../gallery/gallery-viewer';
import { ModalContext } from '@/context/modal-context';
const CustomEditor = dynamic(() => import('../editor/custom-editor'), {
    ssr: false,
});

const initialState = {
    desc: '',
    img: [],
};
function AddRegularForm({ contentId }: { contentId: string }) {
    const galleryImages = useContext(GalleryContext);
    const modalTrigger = useContext(ModalContext);

    const form = useForm({
        defaultValues: {
            regular: [initialState],
        },
    });

    const { fields, append, prepend, remove, swap, move, insert } =
        useFieldArray({
            control: form.control,
            name: 'regular',
        });

    const onSubmit = async (data: any) => {
        const formData = new FormData();
        formData.append(
            'regular',
            JSON.stringify(
                data?.regular?.map((regular: any, index: number) => ({
                    desc: regular?.desc,
                    img: regular?.img,
                    contentId,
                }))
            )
        );
        const res = await createRegularAction(formData);

        toast.success(res?.message);
        modalTrigger?.onClose();
    };

    return (
        <div className="">
            <div className="">
                <h2 className="text-xl font-bold"> Add Descriptions</h2>
                <p className="italic text-end">
                    *All Changes will only reflect after save
                </p>
            </div>
            <Form {...form}>
                <form
                    onSubmit={form.handleSubmit(onSubmit)}
                    className="space-y-10 py-6"
                >
                    {fields.map((field, index) => (
                        <div
                            key={field.id}
                            className="grid gap-8 mb-12 grid-cols-12 pb-8 border-b rounded-md"
                        >
                            <div className="bg-gray-50 col-span-12 flex justify-between items-center p-2">
                                <h1 className="bg-black text-white font-bold text-xl h-8 w-8 flex justify-center items-center rounded-full">
                                    {index + 1}
                                </h1>
                                <Button
                                    size={'sm'}
                                    type="button"
                                    onClick={() => remove(index)}
                                    variant={'destructive'}
                                >
                                    <MinusCircledIcon className="mr-2" />
                                    Remove
                                </Button>
                            </div>
                            <div className="col-span-8 space-y-2 mt-4">
                                <FormLabel>Description</FormLabel>

                                <Controller
                                    control={form.control}
                                    name={`regular.${index}.desc`}
                                    render={({ field }) => (
                                        <CustomEditor
                                            className=" w-full h-[80%] "
                                            value={field.value}
                                            onChange={field.onChange as any}
                                        />
                                    )}
                                />
                            </div>
                            <div className="space-y-4 col-span-4">
                                <div className="flex justify-between">
                                    <FormLabel>Add Gallery Images</FormLabel>
                                    <Badge variant="default">
                                        {
                                            form.watch(`regular.${index}.img`)
                                                ?.length
                                        }{' '}
                                        Selected
                                    </Badge>
                                </div>
                                <div className="max-h-[500px] overflow-auto ">
                                    <Controller
                                        control={form.control}
                                        name={`regular.${index}.img`}
                                        render={({ field }) => (
                                            <GalleryViewerSmall
                                                gallery={galleryImages}
                                                onSelect={field.onChange}
                                                value={field.value as any}
                                                name={`regular.${index}.img`}
                                            />
                                        )}
                                    />
                                </div>
                            </div>
                        </div>
                    ))}

                    <div className="flex justify-end gap-4 items-center">
                        <Button
                            type="button"
                            variant={'outline'}
                            onClick={() => append(initialState)}
                        >
                            <PlusCircledIcon className="mr-2" />
                            Add Description
                        </Button>

                        <Button type="submit">Save</Button>
                    </div>
                </form>
            </Form>
        </div>
    );
}

export default AddRegularForm;
