'use client';
import { Input } from '@/components/ui/input';
import { SubmitButton } from '../ui/submit-button';
import { useFormState } from 'react-dom';
import { toast } from 'sonner';
import { useEffect, useState } from 'react';
import { Label } from '@/components/ui/label';

import { Textarea } from '../ui/textarea';
import {
    addContentAction,
    revalidateDashboardPagesSlug,
} from '@/actions/content.actions';

const initialState = {
    success: null,
    message: null,
    data: null,
};

function SectionContentAddForm({
    pageId,
    onSuccess,
}: {
    pageId: string;
    onSuccess: any;
}) {
    const [state, formAction] = useFormState(
        async (prevState: any, formData: any) => {
            const title = formData.get('title') as string;
            const subTitle = formData.get('subTitle') as string;
            try {
                const res = await addContentAction({
                    pageId,
                    title,
                    subTitle,
                });
                if (res?.success === true) {
                    onSuccess(state?.data?.id);
                    toast.success(res?.message);
                    return res;
                }
                toast.error(res?.message);
                throw new Error(res?.message);
            } catch (err: unknown) {
                return {
                    success: false,
                    message: err,
                };
            }
        },
        initialState
    );



    return (
        <form action={formAction} className="flex flex-col gap-4">
            {/* <div className=" ">
                <Label>Order</Label>
                <Input required name="section" placeholder="Enter Section Order" type='number' />
            </div> */}

            <div>
                <Label>Title</Label>
                <Textarea name="title" placeholder="Enter Title" />
            </div>

            <div>
                <Label>Sub Title</Label>
                <Textarea name="subTitle" placeholder="Enter SubTitle" />
            </div>

            <SubmitButton className="self-end mt-6 w-fit" type="submit">
                Save
            </SubmitButton>
        </form>
    );
}

export default SectionContentAddForm;
