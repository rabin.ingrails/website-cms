'use client';
import { Input } from '@/components/ui/input';
import { SubmitButton } from '../ui/submit-button';
import { useFormState } from 'react-dom';
import { signInAction } from '@/actions/auth.actions';
import { toast } from 'sonner';
import { useEffect } from 'react';
import { redirect } from 'next/navigation';
import Logo from '../logo';
import { Label } from '../ui/label';

const initialState = {
    success: null,
    message: null,
};
function SignInForm() {
    const [state, formAction] = useFormState(signInAction, initialState);

    useEffect(() => {
        if (state?.success === true) {
            toast.success(state.message);
            //*redirect
            redirect('/dashboard');
        } else if (state?.success === false) {
            toast.error(state.message);
        }
    }, [state?.success, state?.message]);

    return (
        <section className="max-w-md mx-auto">
            <Logo className="mx-auto mb-8" />
            <h1 className="text-2xl font-bold text-center mb-6 text-gray-800">
                Sign In to your account
            </h1>

            <form action={formAction}>
                <div className="space-y-4 ">
                    <div>
                        <Label htmlFor="email">Email</Label>
                        <Input
                            required
                            name="email"
                            id="email"
                            placeholder="Enter Email"
                        />
                    </div>
                    <div>
                        <Label htmlFor="password">Password</Label>
                        <Input
                            id="password"
                            required
                            name="password"
                            placeholder="Enter Password"
                        />
                    </div>
                </div>

                <SubmitButton className="w-full mt-8" type="submit">
                    Sign In
                </SubmitButton>
            </form>
        </section>
    );
}

export default SignInForm;
