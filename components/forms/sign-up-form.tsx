'use client';
import { Input } from '@/components/ui/input';
import { SubmitButton } from '../ui/submit-button';
import { useFormState } from 'react-dom';
import { signUpAction } from '@/actions/auth.actions';
import { toast } from 'sonner';
import { useEffect } from 'react';
import { Label } from '../ui/label';
import Logo from '../logo';

const initialState = {
    success: null,
    message: null,
};
function SignUpForm() {
    const [state, formAction] = useFormState(signUpAction, initialState);

    useEffect(() => {
        if (state?.success === true) {
            toast.success(state.message);
        } else if (state?.success === false) {
            toast.error(state.message);
        }
    }, [state?.success, state?.message]);

    return (
        <section className="max-w-md mx-auto">
            <Logo className='mx-auto mb-8' />
            <h1 className="text-2xl font-bold text-center mb-6 text-gray-800" >Create an Account</h1>

            <form action={formAction}>
                <div className="space-y-4 ">
                    <div>
                        <Label htmlFor="projectKey">Project Key</Label>
                        <Input
                            required
                            name="projectKey"
                            id="projectKey"
                            placeholder="Enter Project Key (Eg: cjnepalregion)"
                        />
                    </div>
                    <div>
                        <Label htmlFor="email">Email</Label>
                        <Input
                            required
                            name="email"
                            id="email"
                            placeholder="Enter Email"
                        />
                    </div>
                    <div>
                        <Label htmlFor="password">Password</Label>
                        <Input
                            id="password"
                            required
                            name="password"
                            placeholder="Enter Password"
                        />
                    </div>
                </div>

                <SubmitButton className="w-full mt-5" type="submit">
                    Register
                </SubmitButton>
            </form>
        </section>
    );
}

export default SignUpForm;
