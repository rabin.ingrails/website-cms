'use client';
import React, { useContext, useState } from 'react';
import { Input } from '../ui/input';
import dynamic from 'next/dynamic';
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from '../ui/form';


import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { Button } from '../ui/button';
import { MinusCircledIcon, PlusCircledIcon } from '@radix-ui/react-icons';
import {
    createTabAction,
    removeCardAction,
    removeRegularAction,
    removeTabAction,
    updateCardAction,
    updateRegularAction,
    updateTabAction,
} from '@/actions/content.actions';
import {
    CardContent,
    Content,
    RegularContent,
    TabContent,
} from '@prisma/client';
import { removeTabService } from '@/lib/api';
import { toast } from 'sonner';
import Image from 'next/image';
import { GalleryViewerSmall } from '../gallery/gallery-viewer';
import { GalleryContext } from '@/context/gallery-context';
import { Badge } from '../ui/badge';
const CustomEditor = dynamic(() => import('../editor/custom-editor'), {
    ssr: false,
});
import ModalComponent from '../modal/modal-component';

function EditRegularForm({ regular }: { regular: RegularContent }) {
    console.log('🚀 ~ EditRegularForm ~ regular:', regular);
    const galleryImages = useContext(GalleryContext);

    const form = useForm({
        defaultValues: regular,
    });

    const handleRegularRemove = async () => {
        const res = await removeRegularAction(regular.id);
        toast.success(res?.message);
    };

    const onSubmit = async (data: any, onClose: () => void) => {
        const formData = new FormData();
        formData.append('id', regular.id);
        formData.append('desc', data.desc);
        formData.append('img', JSON.stringify(data.img));
        formData.append('contentId', regular.contentId);
        const res = await updateRegularAction(formData);
        toast.success(res?.message);
        onClose();
    };

    return (
        <>
            <div className="grid grid-cols-12 gap-4 border bg-gray-50 dark:bg-gray-800 rounded-sm p-4 relative">
                <div className="absolute top-2 right-2">
                    <ModalComponent trigger="Edit">
                        {({ onClose }) => (
                            <Form {...form}>
                                <form
                                    onSubmit={form.handleSubmit((data) =>
                                        onSubmit(data, onClose)
                                    )}
                                >
                                    <div className="grid grid-cols-12 gap-4">
                                        <div className=" col-span-7 w-full space-y-2 min-h-[600px]">
                                            <h2 className="">Description</h2>

                                            <Controller
                                                control={form.control}
                                                name={`desc`}
                                                render={({ field }) => (
                                                    <CustomEditor
                                                        className=" w-full h-[80%] "
                                                        value={field.value}
                                                        onChange={
                                                            field.onChange as any
                                                        }
                                                    />
                                                )}
                                            />
                                        </div>
                                        <div className="space-y-4 col-span-5">
                                            <div className="flex justify-between">
                                                <p>Gallery Images</p>
                                                <Badge variant="default">
                                                    {form.watch(`img`)?.length}{' '}
                                                    Selected
                                                </Badge>
                                            </div>
                                            <div className="max-h-[500px] overflow-auto "></div>
                                            <Controller
                                                control={form.control}
                                                name={`img`}
                                                render={({ field }) => (
                                                    <GalleryViewerSmall
                                                        gallery={galleryImages}
                                                        onSelect={
                                                            field.onChange
                                                        }
                                                        value={
                                                            field.value as any
                                                        }
                                                        name={`img`}
                                                    />
                                                )}
                                            />
                                        </div>
                                    </div>

                                    <div className="flex justify-between ">
                                        <Button
                                            type="button"
                                            onClick={() =>
                                                handleRegularRemove()
                                            }
                                            variant={'destructive'}
                                        >
                                            {/* <MinusCircledIcon className="" /> */}
                                            Delete
                                        </Button>
                                            <Button type="submit">Save</Button>
                                    </div>
                                </form>
                            </Form>
                        )}
                    </ModalComponent>
                </div>
                <div className="col-span-12 space-y-2">
                    {/* <small>Desc</small> */}
                    <div className=" max-h-[500px] overflow-auto">
                        <div
                            className="wysiwyg-preview"
                            dangerouslySetInnerHTML={{
                                __html: regular?.desc,
                            }}
                        ></div>
                    </div>
                    <div className="flex gap-4">
                        {regular?.img?.map((i: string) => (
                            <Image
                                src={i}
                                alt={i}
                                width={200}
                                height={200}
                                key={i}
                                className="object-contain"
                            />
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
}

export default EditRegularForm;
