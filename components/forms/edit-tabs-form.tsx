'use client';
import React, { useState } from 'react';
import { Input } from '../ui/input';
import dynamic from 'next/dynamic';
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from '../ui/form';
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from '@/components/ui/dialog';

import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { Button } from '../ui/button';
import { MinusCircledIcon, PlusCircledIcon } from '@radix-ui/react-icons';
import {
    createTabAction,
    removeTabAction,
    updateTabAction,
} from '@/actions/content.actions';
import { Content, TabContent } from '@prisma/client';
import { removeTabService } from '@/lib/api';
const EditorComponent = dynamic(() => import("../editor/editor-component"), { ssr: false });

const initialState = {
    label: '',
    link: '',
    desc: '',
    img: [],
    tabId: '',
};
function EditTabsForm({ tabs }: { tabs: TabContent[] }) {
    const form = useForm({
        defaultValues: {
            tab: tabs.map((i: any) => ({
                tabId: i.id,
                contentId: i.contentId,
                label: i.label,
                link: i.link,
                desc: i.desc,
                img: i.img,
            })),
        },
    });

    const { fields, append, prepend, remove, swap, move, insert } =
        useFieldArray({
            control: form.control,
            name: 'tab',
        });
    const handleTabRemove = async (tabId: string) => {
        console.log('🚀 ~ handleTabRemove ~ tabId:', tabId);
        const res = await removeTabAction(tabId);
        console.log('🚀 ~ handleTabRemove ~ res:', res);
    };

    const onSubmit = async (data: any) => {
        console.log('🚀 ~ onSubmit ~ data:', data);
        const formData = new FormData();
        formData.append(
            'tab',
            JSON.stringify(
                data?.tab?.map((i: any) => ({
                    id: i.id,
                    contentId: i.contentId,
                    label: i.label,
                    link: i.link,
                    desc: i.desc,
                    img: i.img,
                }))
            )
        );
        const res = await updateTabAction(formData);
    };

    return (
        <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)}>
                {fields.map((field, index) => (
                    <div key={field.id} className="space-y-4 ">
                        <Dialog>
                            <DialogTrigger>Edit</DialogTrigger>
                            <DialogContent className="min-w-[60vw] max-h-[80vh] overflow-auto">
                                <DialogHeader>
                                    <DialogTitle>Edit</DialogTitle>
                                    <DialogDescription>
                                        
                                    </DialogDescription>
                                </DialogHeader>
                            </DialogContent>
                        </Dialog>
                        <Button>Remove</Button>

                        <div className="w-full flex items-center gap-5">
                            <FormField
                                control={form.control}
                                name={`tab.${index}.label`}
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Label</FormLabel>
                                        <FormControl>
                                            <Input
                                                placeholder="Label"
                                                {...field}
                                            />
                                        </FormControl>
                                        {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                            <FormField
                                control={form.control}
                                name={`tab.${index}.link`}
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Link</FormLabel>
                                        <FormControl>
                                            <Input
                                                placeholder="Link"
                                                {...field}
                                            />
                                        </FormControl>
                                        {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <Controller
                            control={form.control}
                            name={`tab.${index}.desc`}
                            render={({ field }) => (
                                <EditorComponent
                                    className=" w-full h-[300px] "
                                    value={field.value}
                                    onChange={field.onChange as any}
                                />
                            )}
                        />
                        <Button
                            type="button"
                            onClick={() => handleTabRemove(field.tabId)}
                            variant={'ghost'}
                            size={'icon'}
                        >
                            <MinusCircledIcon className="text-red-500 mt-10" />
                        </Button>
                    </div>
                ))}

                <Button type="submit">Save</Button>
            </form>
        </Form>
    );
}

export default EditTabsForm;
