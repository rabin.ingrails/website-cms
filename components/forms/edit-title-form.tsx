'use client';
import React, { useState } from 'react';
import { Input } from '../ui/input';
import dynamic from 'next/dynamic';
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from '../ui/form';
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from '@/components/ui/dialog';

import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { Button } from '../ui/button';
import { MinusCircledIcon, PlusCircledIcon } from '@radix-ui/react-icons';

import {
    CardContent,
    Content,
    RegularContent,
    TabContent,
} from '@prisma/client';
import { toast } from 'sonner';
import { updateTitleAction } from '@/actions/content.actions';

function EditTitleForm({
    title,
    subTitle,
    contentId,
}: {
    title: string;
    subTitle: string;
    contentId: string;
}) {
    const form = useForm({
        defaultValues: {
            title: title,
            subTitle: subTitle,
        },
    });

    const onSubmit = async (data: any) => {
        const formData = new FormData();
        formData.append('title', data.title);
        formData.append('subTitle', data.subTitle);
        formData.append('contentId', contentId);
        const res = await updateTitleAction(formData);

        toast.success(res?.message);
    };

    return (
        <>
            <div className="grid grid-cols-12 gap-4 border bg-gray-50 dark:bg-gray-800 rounded-sm p-4 ">
                <div className="col-span-11 space-y-2">
                    <div>
                        <small>Title</small>
                        <p className="">{title}</p>
                    </div>
                    <div>
                        <small>Sub Title</small>
                        <p>{subTitle}</p>
                    </div>
                </div>
                <Dialog>
                    <div className="col-span-1">
                        <DialogTrigger asChild>
                            <Button variant="outline">Edit</Button>
                        </DialogTrigger>
                    </div>
                    <DialogContent className="min-w-[60vw] max-h-[80vh] overflow-auto">
                        <DialogHeader>
                            <DialogTitle>Edit</DialogTitle>
                            <DialogDescription>
                                <Form {...form}>
                                    <form
                                        onSubmit={form.handleSubmit(onSubmit)}
                                        className="space-y-4 py-4"
                                    >
                                        <FormField
                                            control={form.control}
                                            name={`title`}
                                            render={({ field }) => (
                                                <FormItem>
                                                    <FormLabel>Title</FormLabel>
                                                    <FormControl>
                                                        <Input
                                                            placeholder="Title"
                                                            {...field}
                                                        />
                                                    </FormControl>

                                                    <FormMessage />
                                                </FormItem>
                                            )}
                                        />
                                        <FormField
                                            control={form.control}
                                            name={`subTitle`}
                                            render={({ field }) => (
                                                <FormItem>
                                                    <FormLabel>
                                                        Sub Title
                                                    </FormLabel>
                                                    <FormControl>
                                                        <Input
                                                            placeholder="Sub Title"
                                                            {...field}
                                                        />
                                                    </FormControl>

                                                    <FormMessage />
                                                </FormItem>
                                            )}
                                        />
                                        <div className="flex justify-end">
                                            <DialogTrigger asChild>
                                                <Button type="submit">
                                                    Save
                                                </Button>
                                            </DialogTrigger>
                                        </div>
                                    </form>
                                </Form>
                            </DialogDescription>
                        </DialogHeader>
                    </DialogContent>
                </Dialog>
            </div>
        </>
    );
}

export default EditTitleForm;
