'use client';
import React, { useContext, useState } from 'react';
import { Input } from '../ui/input';
import dynamic from 'next/dynamic';
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from '../ui/form';
import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { Button } from '../ui/button';
import { MinusCircledIcon, PlusCircledIcon } from '@radix-ui/react-icons';
import { createTabAction } from '@/actions/content.actions';
import { DialogTrigger } from '../ui/dialog';
import { toast } from 'sonner';
import { GalleryContext } from '@/context/gallery-context';
import { Badge } from '../ui/badge';
import { GalleryViewerSmall } from '../gallery/gallery-viewer';
import { ModalContext } from '@/context/modal-context';
const CustomEditor = dynamic(() => import('../editor/custom-editor'), {
    ssr: false,
});

const initialState = {
    label: '',
    link: '',
    desc: '',
    img: [],
};
function AddTabsForm({ contentId }: { contentId: string }) {
    const galleryImages = useContext(GalleryContext);
    const modalTrigger = useContext(ModalContext);

    const form = useForm({
        defaultValues: {
            tab: [initialState],
        },
    });

    const { fields, append, prepend, remove, swap, move, insert } =
        useFieldArray({
            control: form.control,
            name: 'tab',
        });

    const onSubmit = async (data: any) => {
        const formData = new FormData();
        formData.append(
            'tab',
            JSON.stringify(
                data?.tab?.map((tab: any) => ({
                    ...tab,
                    img: tab?.img,
                    contentId,
                }))
            )
        );
        const res = await createTabAction(formData);
        toast.success(res?.message);
        modalTrigger?.onClose();
    };

    return (
        <div className="">
            <div className="">
                <h2 className="text-xl font-bold"> Add Tabs</h2>
                <p className="italic text-end">
                    *All Changes will only reflect after save
                </p>
            </div>
            <Form {...form}>
                <form
                    onSubmit={form.handleSubmit(onSubmit)}
                    className="space-y-10 py-6"
                >
                    {fields.map((field, index) => (
                        <div
                            key={field.id}
                            className="grid gap-8 mb-12 grid-cols-12 pb-8 border-b rounded-md"
                        >
                            <div className="bg-gray-50 col-span-12 flex justify-between items-center p-2">
                                <h1 className="bg-black text-white font-bold text-xl h-8 w-8 flex justify-center items-center rounded-full">
                                    {index + 1}
                                </h1>
                                <Button
                                    size={'sm'}
                                    type="button"
                                    onClick={() => remove(index)}
                                    variant={'destructive'}
                                >
                                    <MinusCircledIcon className="mr-2" />
                                    Remove Tab
                                </Button>
                            </div>
                            <div className="col-span-8">
                                <div className="w-full flex gap-4 ">
                                    <FormField
                                        control={form.control}
                                        name={`tab.${index}.label`}
                                        render={({ field }) => (
                                            <FormItem>
                                                <FormLabel>Label</FormLabel>
                                                <FormControl>
                                                    <Input
                                                        placeholder="Label"
                                                        {...field}
                                                    />
                                                </FormControl>
                                                {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                    <FormField
                                        control={form.control}
                                        name={`tab.${index}.link`}
                                        render={({ field }) => (
                                            <FormItem>
                                                <FormLabel>Link</FormLabel>
                                                <FormControl>
                                                    <Input
                                                        placeholder="Link"
                                                        {...field}
                                                    />
                                                </FormControl>
                                                {/* <FormDescription>
                                                    This is your public display
                                                    name.
                                                </FormDescription> */}
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                </div>
                                <div className="col-span-7 space-y-2 mt-4">
                                    <FormLabel>Description</FormLabel>

                                    <Controller
                                        control={form.control}
                                        name={`tab.${index}.desc`}
                                        render={({ field }) => (
                                            <CustomEditor
                                                className=" w-full  "
                                                value={field.value}
                                                onChange={field.onChange as any}
                                            />
                                        )}
                                    />
                                </div>
                            </div>
                            <div className="space-y-4 col-span-4">
                                <div className="flex justify-between">
                                    <FormLabel>Add Gallery Images</FormLabel>
                                    <Badge variant="default">
                                        {form.watch(`tab.${index}.img`)?.length}{' '}
                                        Selected
                                    </Badge>
                                </div>
                                    <Controller
                                        control={form.control}
                                        name={`tab.${index}.img`}
                                        render={({ field }) => (
                                            <GalleryViewerSmall
                                                gallery={galleryImages}
                                                onSelect={field.onChange}
                                                value={field.value as any}
                                                name={`tab.${index}.img`}
                                            />
                                        )}
                                    />
                            </div>
                        </div>
                    ))}

                    <div className="flex justify-end gap-4 items-center">
                        <Button
                            type="button"
                            variant={'outline'}
                            onClick={() => append(initialState)}
                        >
                            <PlusCircledIcon className="mr-2" />
                            Add Tabs
                        </Button>

                        <Button type="submit">Save</Button>
                    </div>
                </form>
            </Form>
        </div>
    );
}

export default AddTabsForm;
