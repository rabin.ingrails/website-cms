'use client';
import * as React from 'react';

import { useFormStatus } from 'react-dom';
import { Button, ButtonProps, buttonVariants } from './button';
import { cn } from '@/lib/utils';
import { cva, type VariantProps } from 'class-variance-authority';
import { Slot } from '@radix-ui/react-slot';

export const SubmitButton = React.forwardRef<HTMLButtonElement, ButtonProps>(
    (
        { className, variant, size, asChild = false, children, ...props },
        ref
    ) => {
        const { pending } = useFormStatus();
        const Comp = asChild ? Slot : 'button';

        return (
            <Button
                type="submit"
                className={cn(buttonVariants({ variant, size, className }))}
                disabled={pending}
                ref={ref}
            >
                {pending ? 'Loading...' : children}
            </Button>
        );
    }
);

SubmitButton.displayName = 'SubmitButton';
