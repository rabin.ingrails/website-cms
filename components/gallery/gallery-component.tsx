'use client';
import React, { useState } from 'react';
import DropzoneComponent from '../dropzone/dropzone-component';
import { Button } from '../ui/button';

import { toast } from 'sonner';
import { updateGalleryAction } from '@/actions/gallery.actions';
import { FileWithPreview } from '@/types';
import { useUserInfoStore } from '@/store';

const uploadMultipleFiles = async (file: File,projectKey:string) => {
    const response = await fetch(
        process.env.NEXT_PUBLIC_PREFIX
            ? process.env.NEXT_PUBLIC_BASE_URL +
                  '/' +
                  process.env.NEXT_PUBLIC_PREFIX +
                  '/dashboard/api/upload'
            : process.env.NEXT_PUBLIC_BASE_URL + '/dashboard/api/upload',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                filename: file.name,
                contentType: file.type,
                projectKey:projectKey
            }),
        }
    );

    if (response.ok) {
        const { url, fields } = await response.json();
        console.log('🚀 ~ handleFileUpload ~ url:', url, fields);

        const formData = new FormData();
        Object.entries(fields).forEach(([key, value]) => {
            formData.append(key, value as string);
        });
        formData.append('file', file);

        const uploadResponse = await fetch(url, {
            method: 'POST',
            body: formData,
        });

        if (uploadResponse.ok) {
            return {
                success: true,
                fileName: file.name,
                publicUrl: url + fields.key,
            };
        } else {
            console.error(`S3 Upload Error for ${file.name}:`, uploadResponse);
            return { success: false, fileName: file.name };
        }
    } else {
        console.error(`Failed to get pre-signed URL for ${file.name}`);
        return { success: false, fileName: file.name };
    }
};

function GalleryComponent() {
    const [loading, setLoading] = useState(false);
    const [files, setFiles] = useState<FileWithPreview[]>([]);
    const { user } = useUserInfoStore();
    //*perform upload
    const handleFileUpload = async () => {
        if (files.length === 0) {
            toast.error('No files to upload.');
            return;
        }
        setLoading(true);
        const uploadPromises = Array.from(files).map((file) => {
            return uploadMultipleFiles(file,user.projectKey);
        });
        let uploadableUrls: string[] = [];

        const results = await Promise.all(uploadPromises);
        console.log('🚀 ~ handleFileUpload ~ results:', results);

        results.forEach((result) => {
            if (result.success) {
                uploadableUrls.push(result.publicUrl);
            } else {
                toast.error(`Upload failed for ${result.fileName}.`);
            }
        });
        //*store to database
        const formData = new FormData();
        formData.append('gallery', JSON.stringify(uploadableUrls));
        const res = await updateGalleryAction(formData);

        toast.success(res.message);

        setFiles([]);
        setLoading(false);
    };
    return (
        <section className="border flex flex-col  min-h-96 my-6 p-4">
            <div className="flex-1">
                <DropzoneComponent setFiles={setFiles} files={files} />
            </div>
            <Button
                className="w-fit self-end"
                onClick={handleFileUpload}
                disabled={loading}
            >
                {loading ? 'Uploading...' : 'Upload'}
            </Button>
        </section>
    );
}

export default GalleryComponent;
