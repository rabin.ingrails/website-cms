'use client';
import React, { useState } from 'react';
import Image from 'next/image';
import { Gallery, Item } from 'react-photoswipe-gallery';
import { Gallery as GalleryType } from '@prisma/client';
import {
    CheckCircle2,
    CopyIcon,
    ExternalLink,
    X,
    ZoomInIcon,
} from 'lucide-react';
import { Input } from '../ui/input';
import { Button } from '../ui/button';
import { Checkbox } from '../ui/checkbox';
import { toast } from 'sonner';
import { cn } from '@/lib/utils';

const isPDF = (url: string) => url.toLowerCase().endsWith('.pdf');
const isImage = (url: string) =>
    ['.jpg', '.jpeg', '.png', '.gif', '.webp'].some((ext) =>
        url.toLowerCase().endsWith(ext)
    );
function GalleryViewerLarge({
    gallery,
}: {
    gallery: GalleryType[] | undefined;
}) {
    return (
        <Gallery>
            <div className="grid grid-cols-12 gap-4">
                {gallery?.map((item) => {
                    return (
                        <div
                            key={item.id}
                            className="col-span-3 w-full min-h-60 border relative"
                        >
                            <Item
                                original={item.img}
                                thumbnail={item.img}
                                width="1024"
                                height="768"
                            >
                                {({ ref, open }) => (
                                    <>
                                        {!isPDF(item.img) ? (
                                            <Image
                                                ref={ref}
                                                onClick={open}
                                                src={item.img}
                                                alt={item.img}
                                                fill
                                                className="object-contain"
                                            />
                                        ) : (
                                            <div className="relative h-full">
                                                <a
                                                    href={item.img}
                                                    target="_blank"
                                                    className="absolute top-2 right-3 z-10  bg-white rounded p-2 hover:scale-105 "
                                                    onClick={(e) =>
                                                        e.stopPropagation()
                                                    }
                                                >
                                                    <p className="inline-block mr-2">
                                                        Preview
                                                    </p>
                                                    <ExternalLink className="w-4 h-4 inline-block" />
                                                </a>
                                                <iframe
                                                    src={item.img}
                                                    className="w-full h-full"
                                                    title="PDF"
                                                ></iframe>
                                            </div>
                                        )}
                                    </>
                                )}
                            </Item>
                        </div>
                    );
                })}
            </div>
        </Gallery>
    );
}

function GalleryViewerSmall({
    gallery,
    onSelect,
    value,
    name,
}: {
    gallery: GalleryType[] | undefined;
    onSelect: any;
    value: string[];
    name: string;
}) {
    const handleSelection = (img: string) => {
        if (value?.includes(img)) {
            const newValue = value.filter((item) => item !== img);
            onSelect(newValue);
        } else {
            onSelect([...value, img]);
        }
    };
    const copyToClipboard = (text: string) => {
        navigator.clipboard.writeText(text).then(
            () => {
                toast.success('Image URL copied to clipboard');
            },
            (err) => {
                console.error('Could not copy text: ', err);
                toast.error('Failed to copy image URL');
            }
        );
    };

    const [view, setView] = useState({
        id: '',
        open: false,
    });

    return (
        <div className="flex flex-wrap w-full overflow-auto gap-2 max-h-[400px] h-full p-2">
            {gallery?.map((item) => {
                return (
                    <div
                        key={item.id}
                        className={cn(
                            'w-28 bg-gray-100 relative group  rounded-md overflow-hidden ',
                            value?.includes(item.img) &&
                                'outline outline-blue-500'
                        )}
                    >
                        {!isPDF(item.img) ? (
                            <Image
                                src={item.img}
                                alt={item.img}
                                height={200}
                                width={200}
                                onClick={() => {
                                    handleSelection(item.img);
                                }}
                                className="object-contain z-0 relative cursor-pointer  h-24 w-full object-center"
                            />
                        ) : (
                            <div className="relative">
                                <iframe
                                    src={item.img}
                                    className="w-full h-24 relative z-0"
                                    title="PDF"
                                ></iframe>
                                <button
                                    type="button"
                                    className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 bg-black text-white rounded-xl z-30 px-2 py-1"
                                    onClick={() => {
                                        handleSelection(item.img);
                                    }}
                                >
                                    Select PDF
                                </button>
                            </div>
                        )}
                        <div className=" flex justify-end items-center  gap-2  bg-gray-200 p-2 w-full ">
                            <a
                                href={item.img}
                                target="_blank"
                                onClick={(e) => e.stopPropagation()}
                            >
                                <Button
                                    asChild
                                    size={'icon'}
                                    variant={'ghost'}
                                    type="button"
                                    className="w-4 h-4 "
                                    onClick={() => handleSelection(item.img)}
                                >
                                    <ExternalLink />
                                </Button>
                            </a>
                            <Button
                                size={'icon'}
                                variant={'ghost'}
                                type="button"
                                className="w-4 h-4 "
                                onClick={(e) => {
                                    e.stopPropagation();
                                    copyToClipboard(item.img);
                                }}
                            >
                                <CopyIcon />
                            </Button>
                            <Button
                                size={'icon'}
                                type="button"
                                variant={'ghost'}
                                className="w-4 h-4 "
                                onClick={() => {
                                    setView({
                                        id: item.id,
                                        open: !view.open,
                                    });
                                }}
                            >
                                <ZoomInIcon />
                            </Button>
                            {/* <Checkbox
                                checked={value?.includes(item.img)}
                                onCheckedChange={() =>
                                    handleSelection(item.img)
                                }
                            /> */}
                        </div>

                        {/* <div className="absolute top-2 right-2 z-10 ">
                            <Checkbox
                            
                            className=' w-4 h-4 '
                                checked={value?.includes(item.img)}
                                onCheckedChange={() =>
                                    handleSelection(item.img)
                                }
                            />
                        </div> */}
                        {view.id === item.id && view.open && (
                            <div className="fixed  z-40 bg-black/50 top-0 left-0 w-full h-full ">
                                <Image
                                    src={item.img}
                                    alt={item.img}
                                    height={800}
                                    width={1200}
                                    className="object-contain z-50 fixed top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 shadow-2xl"
                                />
                                <Button
                                    variant={'destructive'}
                                    onClick={() =>
                                        setView({ id: '', open: false })
                                    }
                                    className="z-50 fixed top-5 right-5 "
                                >
                                    <X className="h-6 w-6" />
                                </Button>
                            </div>
                        )}
                    </div>
                );
            })}
        </div>
    );
}

export { GalleryViewerLarge, GalleryViewerSmall };
