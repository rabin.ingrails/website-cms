'use client';
import { useUserInfoStore } from '@/store';
import { User } from '@prisma/client';
import React, { useEffect } from 'react';

function UserInfo({
    user,
}: {
    user: Pick<User, 'email' | 'projectKey' | 'id'> | undefined | null;
}) {
    const userStore = useUserInfoStore();
    useEffect(() => {
        if (!user) return;
        userStore.setUser(user);
    }, [user]);

    return <div>{userStore.user.email}</div>;
}

export default UserInfo;
