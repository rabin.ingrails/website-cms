// components/custom-editor.js
'use client'; // only in App Router

import React from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import 'ckeditor5/ckeditor5.css';

import './ck-styles.css';

import {
    ClassicEditor,
    Style,
    Autoformat,
    Bold,
    Italic,
    Underline,
    BlockQuote,
    Base64UploadAdapter,
    CKFinder,
    CKFinderUploadAdapter,
    CloudServices,
    CKBox,
    Essentials,
    Heading,
    Image,
    ImageCaption,
    ImageResize,
    ImageStyle,
    ImageToolbar,
    ImageUpload,
    PictureEditing,
    Indent,
    IndentBlock,
    Link,
    List,
    MediaEmbed,
    Mention,
    Paragraph,
    PasteFromOffice,
    Table,
    TableColumnResize,
    TableToolbar,
    TextTransformation,
    ImageInsert,
    Alignment,
    Code,
    CodeBlock,
} from 'ckeditor5';

function CustomEditor({
    value,
    onChange,
    className
}: {
    className?: string;
    value: any;
    onChange: (value: string) => void;
}) {
    // return (
    //     <CKEditor
    //         editor={ClassicEditor}
    //         config={{
    //             toolbar: {
    //                 items: [
    //                     'heading',
    //                     '|',
    //                     'undo',
    //                     'redo',
    //                     '|',
    //                     'bold',
    //                     'italic',
    //                     '|',
    //                     'alignment',
    //                     'bulletedList',
    //                     'numberedList',
    //                     '|',
    //                     'codeBlock',
    //                 ],
    //             },
    //             plugins: [
    //                 Bold,
    //                 Essentials,
    //                 Italic,
    //                 Mention,
    //                 Paragraph,
    //                 Heading,
    //                 Alignment,
    //                 CodeBlock,
    //                 List,
    //                 Autoformat,
    //             ],
    //             heading: {
    //                 options: [
    //                     {
    //                         model: 'paragraph',
    //                         title: 'Paragraph',
    //                         class: 'ck-heading_paragraph',
    //                     },
    //                     {
    //                         model: 'heading1',
    //                         view: 'h1',
    //                         title: 'Heading 1',
    //                         class: 'ck-heading_heading1',
    //                     },
    //                     {
    //                         model: 'heading2',
    //                         view: 'h2',
    //                         title: 'Heading 2',
    //                         class: 'ck-heading_heading2',
    //                     },
    //                     {
    //                         model: 'heading3',
    //                         view: 'h3',
    //                         title: 'Heading 3',
    //                         class: 'ck-heading_heading3',
    //                     },
    //                     {
    //                         model: 'heading4',
    //                         view: 'h4',
    //                         title: 'Heading 4',
    //                         class: 'ck-heading_heading4',
    //                     },
    //                 ],
    //             },

    //             initialData: '<p>Hello from CKEditor 5 in React!</p>',
    //         }}
    //     />
    // );
    return (
        <CKEditor
            editor={ClassicEditor}
            config={{
                toolbar: {
                    items: [
                        'undo',
                        'redo',
                        '|',
                        'heading',
                        '|',
                        'fontfamily',
                        'fontsize',
                        'fontColor',
                        'fontBackgroundColor',
                        '|',
                        'bold',
                        'italic',
                        'strikethrough',
                        'subscript',
                        'superscript',
                        'code',
                        '|',
                        'link',
                        'insertImageViaUrl',
                        'blockQuote',
                        '|',
                        'alignment',
                        '|',
                        'bulletedList',
                        'numberedList',
                        'todoList',
                        // '|',
                        // 'outdent', 'indent',
                        '|',
                        'insertTable',
                        '|',
                        'mediaEmbed',
                    ],
                    shouldNotGroupWhenFull: true,
                },
                plugins: [
                    Autoformat,
                    Alignment,
                    BlockQuote,
                    Bold,
                    // CKFinder,
                    // CKFinderUploadAdapter,
                    // CloudServices,
                    Essentials,
                    Heading,
                    Image,
                    ImageInsert,
                    ImageCaption,
                    ImageResize,
                    ImageStyle,
                    ImageToolbar,
                    // ImageUpload,
                    // Base64UploadAdapter,
                    Indent,
                    IndentBlock,
                    Italic,
                    Link,
                    List,
                    MediaEmbed,
                    // Mention,
                    Paragraph,
                    PasteFromOffice,
                    PictureEditing,
                    Table,
                    TableColumnResize,
                    TableToolbar,
                    TextTransformation,
                    Underline,
                    Table,
                    CodeBlock,
                ],

                heading: {
                    options: [
                        {
                            model: 'paragraph',
                            title: 'Paragraph',
                            class: 'ck-heading_paragraph',
                        },
                        {
                            model: 'heading1',
                            view: 'h1',
                            title: 'Heading 1',
                            class: 'ck-heading_heading1',
                        },
                        {
                            model: 'heading2',
                            view: 'h2',
                            title: 'Heading 2',
                            class: 'ck-heading_heading2',
                        },
                        {
                            model: 'heading3',
                            view: 'h3',
                            title: 'Heading 3',
                            class: 'ck-heading_heading3',
                        },
                        {
                            model: 'heading4',
                            view: 'h4',
                            title: 'Heading 4',
                            class: 'ck-heading_heading4',
                        },
                    ],
                },

                image: {
                    resizeOptions: [
                        {
                            name: 'resizeImage:original',
                            label: 'Default image width',
                            value: null,
                        },
                        {
                            name: 'resizeImage:50',
                            label: '50% page width',
                            value: '50',
                        },
                        {
                            name: 'resizeImage:75',
                            label: '75% page width',
                            value: '75',
                        },
                    ],
                    toolbar: [
                        'imageTextAlternative',
                        'toggleImageCaption',
                        '|',
                        'imageStyle:inline',
                        'imageStyle:wrapText',
                        'imageStyle:breakText',
                        '|',
                        'resizeImage',
                    ],
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells',
                    ],
                },

                // initialData: '<p>Hello from CKEditor 5 in React!</p>',
            }}
            onChange={(event, editor) => {
                const data = editor.getData();

                onChange(data);
            }}
            data={value}
            
        />
    );
}

export default CustomEditor;
