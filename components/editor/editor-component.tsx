import React, { useMemo, useRef } from 'react';
import dynamic from 'next/dynamic';
import 'react-quill/dist/quill.snow.css';

var toolbarOptions = [
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],

    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction

    // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown

    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],

    ['clean'],                                         // remove formatting button
    ['image']
];

const ReactQuill = dynamic(
    async () => {
        const ReactQuill = (await import('react-quill')).default;

        // eslint-disable-next-line react/display-name
        return ({ forwardedRef, ...rest }: any) => (
            <ReactQuill ref={forwardedRef} {...rest} />
        );
    },
    {
        ssr: false,
    }
);
const Editor = ({
    value,
    onChange,
    className
}: {
    value: string;
    onChange: (value: string) => void;
    className?: string
}) => {
    const quillRef = useRef<null>(null);
    const modules = useMemo(
        () => ({
            toolbar: {
                container: toolbarOptions,
                handlers: {
                    image: imageHandler,
                },
            },
        }),
        []
    );

    function imageHandler() {
        if (!quillRef.current) return;

        const editor = (quillRef.current as any)?.getEditor();
        const range = editor.getSelection();
        const value = prompt('Please enter the image URL');

        if (value && range) {
            editor.insertEmbed(range.index, 'image', value, 'user');
        }
    }

    return (
        <ReactQuill
            forwardedRef={quillRef}
            theme="snow"
            defaultValue={value}
            onChange={onChange}
            modules={modules}
        />
    );
};

export default Editor;
