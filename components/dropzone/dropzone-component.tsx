'use client';

import { cn } from '@/lib/utils';
import { FileWithPreview } from '@/types';
import { Cross1Icon, Cross2Icon } from '@radix-ui/react-icons';
import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import Dropzone, { DropzoneProps } from 'react-dropzone';


export default function DropzoneComponent({
    files,
    setFiles
}: {
  files: FileWithPreview[];
setFiles: React.Dispatch<React.SetStateAction<FileWithPreview[]>>
}) {
    const handleFilesAccepted = (acceptedFiles: File[]) => {
        setFiles(
            (prev)=>
                 [ ...prev,
            ...acceptedFiles?.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file),
                })
            )
        ]
        );
    };
    const handleFileRemoveByIndex = (index: number) => {
        setFiles((prev) => prev.filter((_, i) => i !== index));
    }
    const thumbs = files?.map((file,index) => (
            <div className='border rounded-lg flex items-center justify-center relative' key={file.name}>
                <Image
                className=' object-cover rounded-lg'
                height={120}
                width={120}
                alt={file.name}
                    src={file.preview}
                    // Revoke data uri after image is loaded
                    onLoad={() => {
                        URL.revokeObjectURL(file.preview);
                    }}
                />
                <Cross2Icon className='absolute top-0 right-0 z-30 hover:bg-red-300 cursor-pointer bg-red-200 block border rounded-full h-4 w-4' 
                onClick={() => handleFileRemoveByIndex(index)}
                />
            </div>
    ));

    useEffect(() => {
        // Make sure to revoke the data uris to avoid memory leaks, will run on unmount
        return () => files.forEach((file) => URL.revokeObjectURL(file.preview));
    }, []);

    return (
        <>
            <Dropzone onDrop={handleFilesAccepted}>
                {({ getRootProps, getInputProps, isDragActive }) => (
                    <section className="">
                        <div {...getRootProps()} className={cn("text-center border-dashed border-2 p-10", isDragActive && "bg-blue-50")}>
                            <input {...getInputProps()} />
                            {isDragActive ? (
                                <p>Drop the files here ...</p>
                            ) : (
                                <p>
                                    Drag n drop some files here, or click to
                                    select files
                                </p>
                            )}
                        </div>
                    </section>
                )}
            </Dropzone>
            <aside className='flex gap-2 mt-4 flex-wrap'>{thumbs}</aside>
        </>
    );
}
